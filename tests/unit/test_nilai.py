from webapp.models import NilaiDasar, NilaiTerapan, NilaiPkm

""" This test not accessing the underlying production database
its only check the interface class used by SQLAlchemy"""

def test_add_nilaidasar():
    """
    GIVEN a NilaiDasar model
    WHEN a new nilai is created
    THEN check the nrumusanmslh, nluaran dst are defined correctly
    """
    nilai = NilaiDasar(nrumusanmslh=80, nluaran=76, npustaka=78, nmetode=83, nkelayakan=80, ntotal=80)
    assert nilai.nrumusanmslh == 80
    assert nilai.nluaran == 76
    assert nilai.npustaka == 78
    assert nilai.nmetode == 83
    assert nilai.nkelayakan == 80
    assert nilai.ntotal == 80

def test_add_nilaiterap():
    """
    GIVEN a NilaiTerapan model
    WHEN a new nilai is created
    THEN check the nroadmap, nluaran, npustaka dsb are defined correctly
    """
    nilai = NilaiTerapan(nroadmap=80, nluaran=76, npustaka=78, nmetode=83, nkelayakan=80, ntotal=80)
    assert nilai.nroadmap == 80
    assert nilai.nluaran == 76
    assert nilai.npustaka == 78
    assert nilai.nmetode == 83
    assert nilai.nkelayakan == 80
    assert nilai.ntotal == 80 

def test_add_nilaipkm():
    """
    GIVEN a NilaiPkm model
    WHEN a new nilai is created
    THEN check the nsituasi, nmitra dsb are defined correctly
    """
    nilai = NilaiPkm(nsituasi=80, nmitra=76, nsolusi=78, nluaran=83, nkelayakan=80, nbiaya=76, ntotal=80)
    assert nilai.nsituasi == 80
    assert nilai.nmitra == 76
    assert nilai.nsolusi == 78
    assert nilai.nluaran == 83
    assert nilai.nkelayakan == 80
    assert nilai.nbiaya == 76
    assert nilai.ntotal == 80 