from webapp.models import Users

""" This test not accessing the underlying production database
its only check the interface class used by SQLAlchemy"""

def test_new_users():
    """
    GIVEN a User model
    WHEN a new User is created
    THEN check the username, email, and name fields are defined correctly
    """
    user = Users(username='subagyo1887', email='sub_bagyo@bagyo.com', name='subagyooygabus')
    assert user.username == 'subagyo1887'
    assert user.email == 'sub_bagyo@bagyo.com'
    assert user.name == 'subagyooygabus'
    
def test_update_users():
    """
    GIVEN a User model
    WHEN a new User is updated/edited
    THEN check the username, email, and name fields are defined correctly
    """
    user = Users(username='andiwarhol', email='andy@warhol.com', name='Andy Warhol')
    assert user.username == 'andiwarhol'
    assert user.email == 'andy@warhol.com'
    assert user.name == 'Andy Warhol'

