from webapp.models import Pengumuman

""" This test not accessing the underlying production database
its only check the interface class used by SQLAlchemy"""

def test_add_pengumuman():
    """
    GIVEN a Pengumuman model
    WHEN a new Pengumuman is created
    THEN check the judul, isi, filename are defined correctly
    """
    pengumuman = Pengumuman(judul="Pengumuman percobaan", isi="Lorem ipsum dolor sit amet", filename="d2gb1bdi1be1jbjkbiubdasiudsab87tdiauy8.pdf")
    assert pengumuman.judul == "Pengumuman percobaan"
    assert pengumuman.isi == "Lorem ipsum dolor sit amet"
    assert pengumuman.filename == "d2gb1bdi1be1jbjkbiubdasiudsab87tdiauy8.pdf"

def test_set_status_pengumuman():
    """
    GIVEN a Pengumuman model
    WHEN a new Pengumuman is created
    THEN check the isactive are defined correctly
    """
    pengumuman = Pengumuman(isactive='Ya')
    assert pengumuman.isactive == 'Ya'
