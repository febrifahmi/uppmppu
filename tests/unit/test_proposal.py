from webapp.models import ProposalLit, ProposalPkm, PublikasiHKI

""" This test not accessing the underlying production database
its only check the interface class used by SQLAlchemy"""

def test_add_proposallit():
    """
    GIVEN a ProposalLit model
    WHEN a new Proposal is created
    THEN check the judul, biaya, tahun, anggota, kategori, tema, koderumpun, namarumpun, qrcodestring, filename are defined correctly
    """
    proposal = ProposalLit(judul="Dummy judul", biaya=100000000, tahun=2021, anggota="A, B, C, D", kategori="Dasar", tema="BIM", koderumpun=2134, namarumpun="Teknik Sipil", qrcodestring="321tiub2k1jbkjb1b231jb3k12jb31k2v312k3j12bkj321k", filename="321tiub2k1jbkjb1b231jb3k12jb31k2v312k3j12bkj321k.pdf")
    assert proposal.judul == "Dummy judul"
    assert proposal.biaya == 100000000
    assert proposal.tahun == 2021
    assert proposal.anggota == "A, B, C, D"
    assert proposal.kategori == "Dasar"
    assert proposal.tema == "BIM"
    assert proposal.koderumpun == 2134
    assert proposal.namarumpun == "Teknik Sipil"
    assert proposal.qrcodestring == "321tiub2k1jbkjb1b231jb3k12jb31k2v312k3j12bkj321k"
    assert proposal.filename == "321tiub2k1jbkjb1b231jb3k12jb31k2v312k3j12bkj321k.pdf"

def test_add_proposallit_crafted_input():
    """
    GIVEN a ProposalLit model
    WHEN a new Proposal is created
    THEN check the judul, biaya, tahun, anggota, kategori, tema, koderumpun, namarumpun, qrcodestring, filename are defined correctly WHEN users deliberately inputting illegal characters
    """
    proposal = ProposalLit(judul="Dummy judul ' OR = 1;", biaya=100000000, tahun=2021, anggota="A, B, C, D", kategori="Dasar", tema="BIM", koderumpun=2134, namarumpun="Teknik Sipil", qrcodestring="321tiub2k1jbkjb1b231jb3k12jb31k2v312k3j12bkj321k", filename="321tiub2k1jbkjb1b231jb3k12jb31k2v312k3j12bkj321k.rar")
    assert proposal.judul == "Dummy judul ' OR = 1;"
    assert proposal.biaya == 100000000
    assert proposal.tahun == 2021
    assert proposal.anggota == "A, B, C, D"
    assert proposal.kategori == "Dasar"
    assert proposal.tema == "BIM"
    assert proposal.koderumpun == 2134
    assert proposal.namarumpun == "Teknik Sipil"
    assert proposal.qrcodestring == "321tiub2k1jbkjb1b231jb3k12jb31k2v312k3j12bkj321k"
    assert proposal.filename == "321tiub2k1jbkjb1b231jb3k12jb31k2v312k3j12bkj321k.rar"

def test_add_proposalpkm():
    """
    GIVEN a ProposalPkm model
    WHEN a new Proposal is created
    THEN check the judul, biaya, tahun, anggota, qrcodestring, filename are defined correctly
    """
    proposal = ProposalPkm(judul="Dummy judul PkM", biaya=100000000, tahun=2021, anggota="A, B, C, D", qrcodestring="321tiub2k1jbkjb1b231jb3k12jb31k2v312k3j12bkj321k", filename="321tiub2k1jbkjb1b231jb3k12jb31k2v312k3j12bkj321k.pdf")
    assert proposal.judul == "Dummy judul PkM"
    assert proposal.biaya == 100000000
    assert proposal.tahun == 2021
    assert proposal.anggota == "A, B, C, D"
    assert proposal.qrcodestring == "321tiub2k1jbkjb1b231jb3k12jb31k2v312k3j12bkj321k"
    assert proposal.filename == "321tiub2k1jbkjb1b231jb3k12jb31k2v312k3j12bkj321k.pdf"
    
def test_add_pub_hki():
    """
    GIVEN a PublikasiHKI model
    WHEN a new Proposal is created
    THEN check the judul, biaya, keterangan, filename are defined correctly
    """
    proposal = PublikasiHKI(judul="Dummy pengajuan pembiayaan publikasi dan HKI", biaya=100000000, filename="dhoih21oih39h1oib12oi31h3891h31.pdf")
    assert proposal.judul == "Dummy pengajuan pembiayaan publikasi dan HKI"
    assert proposal.biaya == 100000000
    assert proposal.filename == "dhoih21oih39h1oib12oi31h3891h31.pdf"
