import pytest
from flask import current_app

from webapp import app

@pytest.fixture
def client():
    with app.test_client() as client:
        with app.app_context():  # New!!
            assert current_app.config["SECRET_KEY"] == "you-will-never-guess"
        yield client

# test using client from fixture 
def test_index(client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/' page is requested (GET)
    THEN check that the response is valid (cek that return render_template success)
    """
    response = client.get('/')
    assert response.status_code == 200
    assert b'Welcome' in response.data

def test_register_page(client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/register' page is requested (GET)
    THEN check that the response is valid (cek that return render_template success)
    """
    response = client.get('/register')
    assert response.status_code == 200
    assert b'Register' in response.data
    
def test_login_page(client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/login' page is requested (GET)
    THEN check that the response is valid (cek that return render_template success)
    """
    response = client.get('/login')
    assert response.status_code == 200
    assert b'Login' in response.data

def test_logout_page(client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/logout' page is requested (GET)
    THEN check that the response is valid (cek that return render_template success) for anonymous users
    """
    response = client.get('/logout')
    # as it has @login_required decorator the resp would be 302 if user not signed in
    assert response.status_code == 302
    
