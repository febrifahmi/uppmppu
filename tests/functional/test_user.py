import pytest
from flask import current_app

from webapp import app

@pytest.fixture
def client():
    with app.test_client() as client:
        with app.app_context():  # New!!
            assert current_app.config["SECRET_KEY"] == "you-will-never-guess"
        yield client

