# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2021-02-20
### Added
- New CHANGELOG.md to track the changes of the project.
- Adding duplicate filename checking on proposal upload.
- Adding edit pengumuman.
- Fixing validasi input penilaian and updating readme.
- Revisi penilaian dengan kategori baru fixed.

### Changed
- Change placeholder text styling to smaller text in search input placeholder.
- Small revision on user dashboard (catatan).

### Removed
- (N/A)