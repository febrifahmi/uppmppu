from webapp import app
from webapp.models import Users, ProposalLit

@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'Users': Users, 'ProposalLit': ProposalLit}