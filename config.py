import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SECRET_KEY = os.environ.get("SECRET_KEY") or "you-will-never-guess"
    # SQL Alchemy config
    SQLALCHEMY_DATABASE_URI = os.environ.get(
        "DATABASE_URL"
    ) or "sqlite:///" + os.path.join(basedir, "app.db")
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # set server/domain name tld used for routes.py to find the exact domain url of files
    # SERVER_NAME = 'localhost:5000'
    UPLOAD_DIRECTORY = basedir + "/webapp/uploads"
    DOWNLOAD_DIRECTORY = basedir + "/webapp/downloads"
    LOG_DIRECTORY = basedir + "/logs"
    ACCEPTED_FILE_TYPES = ["doc", "docx", "odt", "pdf", "rtf", "png", "jpg"]
    # pagination config
    POSTS_PER_PAGE = 10
    # basedir
    BASEDIR = basedir
    # recaptcha
    RECAPTCHA_USE_SSL = False
    RECAPTCHA_PUBLIC_KEY = "your key here"
    RECAPTCHA_PRIVATE_KEY = "your key here"
    RECAPTCHA_OPTIONS = {"theme": "white"}
