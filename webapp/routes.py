import os, random, string, requests
from flask import (
    render_template,
    flash,
    redirect,
    url_for,
    request,
    send_file,
    send_from_directory,
    make_response,
)
from webapp import app, db, qrcode
from webapp.forms import (
    LoginForm,
    RegistrationForm,
    AddPropLit,
    AddPropPkm,
    AddPropPubHKI,
    UpdatePropLit,
    UpdatePropPkm,
    UpdatePropPubHKI,
    AdmAddUsers,
    AdmEditUsers,
    AdmBuatPengumuman,
    AdmEditPengumuman,
    AdmUploadFile,
    SetStatusProp,
    SkorPropDasarForm,
    SkorPropTerapanForm,
    SkorPropPengembanganForm,
    SkorPropPkmForm,
    EditSkorPropDasarForm,
    EditSkorPropTerapanForm,
    EditSkorPropPengembanganForm,
    EditSkorPropPkmForm,
)
from flask_login import current_user, login_user, logout_user, login_required
from webapp.models import (
    Users,
    ProposalLit,
    ProposalPkm,
    PublikasiHKI,
    Pengumuman,
    SkorPropDasar,
    SkorPropTerapan,
    SkorPropPengembangan,
    SkorPropPkm,
)
from werkzeug.urls import url_parse
from werkzeug.utils import secure_filename
import pdfkit
import pandas as pd
import flask_excel as excel
from datetime import datetime


basedir = os.path.abspath(os.path.dirname(__file__))

# LOGIN LOGOUT AND REGISTER
@app.route("/", methods=["GET", "POST"])
@app.route("/index", methods=["GET", "POST"])
def index():
    if current_user.is_authenticated:
        if current_user.admin == "Ya":
            next_page = url_for("admdashboard")
        elif current_user.togglepenilai == "Ya" and current_user.admin == "Tidak":
            next_page = url_for("pendashboard")
        else:
            next_page = url_for("dashboard")
        return redirect(next_page)
    form = LoginForm()
    if form.validate_on_submit():
        user = Users.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash("Invalid username or password")
            return redirect(url_for("index"))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get("next")
        if not next_page or url_parse(next_page).netloc != "":
            if current_user.admin == "Ya":
                next_page = url_for("admdashboard")
            elif current_user.togglepenilai == "Ya" and current_user.admin == "Tidak":
                next_page = url_for("pendashboard")
            else:
                next_page = url_for("dashboard")
        return redirect(next_page)
    return render_template("newindex.html", title="Welcome", form=form)


@app.route("/login", methods=["GET", "POST"])
def login():
    if current_user.is_authenticated:
        if current_user.admin == "Ya":
            next_page = url_for("admdashboard")
        elif current_user.togglepenilai == "Ya" and current_user.admin == "Tidak":
            next_page = url_for("pendashboard")
        else:
            next_page = url_for("dashboard")
        return redirect(next_page)
    form = LoginForm()
    if form.validate_on_submit():
        user = Users.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash("Invalid username or password")
            return redirect(url_for("index"))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get("next")
        if not next_page or url_parse(next_page).netloc != "":
            if current_user.admin == "Ya":
                next_page = url_for("admdashboard")
            elif current_user.togglepenilai == "Ya" and current_user.admin == "Tidak":
                next_page = url_for("pendashboard")
            else:
                next_page = url_for("dashboard")
        return redirect(next_page)
    return render_template("login.html", title="Login", form=form)


@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for("index"))


@app.route("/register", methods=["GET", "POST"])
def register():
    if current_user.is_authenticated:
        return redirect(url_for("dashboard"))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = Users(
            username=form.username.data,
            email=form.email.data,
            name=form.nama.data,
            role=form.posisi.data,
        )
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash("Congratulations, you are now a registered user!")
        return redirect(url_for("index"))
    return render_template("register.html", title="Register", form=form)


# PENGUMUMAN FOR ALL USERS
@app.route("/pengumuman")
def pengumuman():
    # page = request.args.get('page', 1, type=int)
    allpengumuman = (
        Pengumuman.query.order_by(Pengumuman.created_at.desc()).limit(100).all()
    )
    baseurl = os.path.join(basedir, "uploads")
    return render_template(
        "pengumuman.html", title="Pengumuman", pengumuman=allpengumuman, baseurl=baseurl
    )


# MAIN DASHBOARD USERS
@app.route("/dashboard")
@login_required
def dashboard():
    alllit = ProposalLit.query.all()
    allpkm = ProposalPkm.query.all()
    allpengumumanaktif = (
        Pengumuman.query.order_by(Pengumuman.created_at.desc())
        .filter_by(isactive="Ya")
        .limit(5)
        .all()
    )
    allnilailit = []
    # allnilaidasar = NilaiDasar.query.all()
    # allnilaiterap = NilaiTerapan.query.all()
    allskorlitdasar = SkorPropDasar.query.all()
    allskorlitterap = SkorPropTerapan.query.all()
    allskorlitbang = SkorPropPengembangan.query.all()
    for item in alllit:
        if item.kategori == "Dasar":
            for n in allskorlitdasar:
                if n.fk_proposal_id == item.idproposallit:
                    allnilailit.append(n)
        elif item.kategori == "Terapan":
            for n in allskorlitterap:
                if n.fk_proposal_id == item.idproposallit:
                    allnilailit.append(n)
        elif item.kategori == "Pengembangan":
            for n in allskorlitbang:
                if n.fk_proposal_id == item.idproposallit:
                    allnilailit.append(n)
    return render_template(
        "dashboard.html",
        title="Dashboard",
        penelitian=alllit,
        pkm=allpkm,
        pengumuman=allpengumumanaktif,
        nilai=allnilailit,
    )


# PENGAJUAN PROPOSAL PENELITIAN
## Daftar prop penelitian
@app.route("/dashboard/daftarlit", methods=["GET", "POST"])
@login_required
def daftarlit():
    form = AddPropLit()
    if request.method == "POST":
        form.validate_on_submit()
        proposal = ProposalLit(
            judul=form.judul.data,
            biaya=form.biaya.data,
            tahun=form.tahun.data,
            anggota=form.anggota.data,
            kategori=form.kategori.data,
            tema=form.tema.data,
            fk_ketua_id=current_user.idusers,
        )
        f = form.urifile.data
        ceklocalfiles = []
        # cek apakah file yang diupload sesuai daftar jenis file yg diijinkan
        if (
            "." in f.filename
            and f.filename.rsplit(".", 1)[1] in app.config["ACCEPTED_FILE_TYPES"]
        ):
            filename = secure_filename(f.filename)
            origext = os.path.splitext(filename)[1]
            rndfilename = (
                "".join(random.choices(string.ascii_letters + string.digits, k=32))
                + origext
            )
            # kode baru untuk cek local file names
            cekprop = ProposalLit.query.all()
            for item in cekprop:
                if rndfilename == item.filename:
                    rndfilename = (
                        "".join(
                            random.choices(string.ascii_letters + string.digits, k=32)
                        )
                        + origext
                    )
                    break
                else:
                    pass
            f.save(os.path.join(basedir, "uploads", rndfilename))
            path = os.path.join(basedir, "uploads", rndfilename)
            proposal.filename = rndfilename
            proposal.setQRcode(path)
            print(path)
            db.session.add(proposal)
            db.session.commit()
            return redirect(url_for("lstpenelitian"))
        else:
            flash(
                "File (%s) is not an accepted format. Please upload .docx or .pdf files instead"
                % f.filename
            )
            print(f.filename)
    return render_template(
        "daftarlit.html", title="Pengajuan proposal penelitian", form=form
    )


## List prop penelitian
@app.route("/dashboard/lstpenelitian")
@login_required
def lstpenelitian():
    # page = request.args.get("page", 1, type=int)
    # penelitian = ProposalLit.query.paginate(page, app.config["POSTS_PER_PAGE"], False)
    penelitian = ProposalLit.query.all()
    # next_url = (
    #     url_for("lstpenelitian", page=penelitian.next_num)
    #     if penelitian.has_next
    #     else None
    # )
    # prev_url = (
    #     url_for("lstpenelitian", page=penelitian.prev_num)
    #     if penelitian.has_prev
    #     else None
    # )
    return render_template(
        "lstpenelitian.html",
        title="Daftar proposal penelitian",
        penelitian=penelitian,
        # penelitian=penelitian.items,
        # next_url=next_url,
        # prev_url=prev_url,
    )


## Update/edit prop penelitian
@app.route("/dashboard/updatelit/<id>", methods=["GET", "POST"])
@login_required
def updatelit(id):
    form = UpdatePropLit()
    currentproposal = ProposalLit.query.filter_by(idproposallit=id).first()
    if form.validate_on_submit():
        currentproposal.judul = form.judul.data
        currentproposal.biaya = form.biaya.data
        currentproposal.tahun = form.tahun.data
        currentproposal.anggota = form.anggota.data
        currentproposal.kategori = form.kategori.data
        currentproposal.tema = form.tema.data
        # currentproposal.koderumpun = form.koderumpun.data
        # currentproposal.namarumpun = form.namarumpun.data
        f = form.urifile.data
        # cek apakah file yang diupload sesuai daftar jenis file yg diijinkan
        if (
            "." in f.filename
            and f.filename.rsplit(".", 1)[1] in app.config["ACCEPTED_FILE_TYPES"]
        ):
            filename = secure_filename(f.filename)
            origext = os.path.splitext(filename)[1]
            rndfilename = (
                "".join(random.choices(string.ascii_letters + string.digits, k=32))
                + origext
            )
            # kode baru untuk cek local file names
            cekprop = ProposalLit.query.all()
            for item in cekprop:
                if rndfilename == item.filename:
                    rndfilename = (
                        "".join(
                            random.choices(string.ascii_letters + string.digits, k=32)
                        )
                        + origext
                    )
                    break
                else:
                    pass
            f.save(os.path.join(basedir, "uploads", rndfilename))
            path = os.path.join(basedir, "uploads", rndfilename)
            currentproposal.filename = rndfilename
            currentproposal.setQRcode(path)
            print(path)
            db.session.commit()
            return redirect(url_for("lstpenelitian"))
        else:
            flash(
                "File (%s) is not an accepted format. Please upload .docx or .pdf files instead"
                % f.filename
            )
            print(f.filename)
    if request.method == "GET":
        form.judul.data = currentproposal.judul
        form.biaya.data = currentproposal.biaya
        form.tahun.data = currentproposal.tahun
        form.anggota.data = currentproposal.anggota
        form.kategori.data = currentproposal.kategori
        form.tema.data = currentproposal.tema
        # form.koderumpun.data = currentproposal.koderumpun
        # form.namarumpun.data = currentproposal.namarumpun
    return render_template(
        "updatelit.html", title="Update proposal penelitian", form=form
    )


## Delete prop penelitian (todo)
@app.route("/dashboard/deletelit/<id>")
@login_required
def deletelit(id):
    selectedprop = ProposalLit.query.filter_by(idproposallit=id).first()
    db.session.delete(selectedprop)
    db.session.commit()
    return redirect(url_for("lstpenelitian"))


# PENGAJUAN PROPOSAL PkM
## Daftar PkM
@app.route("/dashboard/daftarpkm", methods=["GET", "POST"])
@login_required
def daftarpkm():
    form = AddPropPkm()
    if request.method == "POST":
        form.validate_on_submit()
        proposal = ProposalPkm(
            judul=form.judul.data,
            biaya=form.biaya.data,
            tahun=form.tahun.data,
            anggota=form.anggota.data,
            fk_ketua_id=current_user.idusers,
        )
        f = form.urifile.data
        # cek apakah file yang diupload sesuai daftar jenis file yg diijinkan
        if (
            "." in f.filename
            and f.filename.rsplit(".", 1)[1] in app.config["ACCEPTED_FILE_TYPES"]
        ):
            filename = secure_filename(f.filename)
            origext = os.path.splitext(filename)[1]
            rndfilename = (
                "".join(random.choices(string.ascii_letters + string.digits, k=32))
                + origext
            )
            # kode baru untuk cek local file names
            cekprop = ProposalPkm.query.all()
            for item in cekprop:
                if rndfilename == item.filename:
                    rndfilename = (
                        "".join(
                            random.choices(string.ascii_letters + string.digits, k=32)
                        )
                        + origext
                    )
                    break
                else:
                    pass
            f.save(os.path.join(basedir, "uploads", rndfilename))
            path = os.path.join(basedir, "uploads", rndfilename)
            proposal.filename = rndfilename
            proposal.setQRcode(path)
            print(path)
            db.session.add(proposal)
            db.session.commit()
            return redirect(url_for("lstpkm"))
        else:
            flash(
                "File (%s) is not an accepted format. Please upload .docx or .pdf files instead"
                % f.filename
            )
            print(f.filename)
    return render_template(
        "daftarpkm.html",
        title="Pengajuan proposal pengabdian kepada masyarakat",
        form=form,
    )


## List PkM
@app.route("/dashboard/lstpkm")
@login_required
def lstpkm():
    # page = request.args.get("page", 1, type=int)
    # pkm = ProposalPkm.query.paginate(page, app.config["POSTS_PER_PAGE"], False)
    pkm = ProposalPkm.query.all()
    # next_url = url_for("lstpkm", page=pkm.next_num) if pkm.has_next else None
    # prev_url = url_for("lstpkm", page=pkm.prev_num) if pkm.has_prev else None
    return render_template(
        "lstpkm.html",
        title="Daftar proposal pengabdian kepada masyarakat",
        pkm=pkm,
        # pkm=pkm.items,
        # next_url=next_url,
        # prev_url=prev_url,
    )


## Update/edit prop PkM
@app.route("/dashboard/updatepkm/<id>", methods=["GET", "POST"])
@login_required
def updatepkm(id):
    form = UpdatePropPkm()
    currentproposal = ProposalPkm.query.filter_by(idproposalpkm=id).first()
    if form.validate_on_submit():
        currentproposal.judul = form.judul.data
        currentproposal.biaya = form.biaya.data
        currentproposal.tahun = form.tahun.data
        currentproposal.anggota = form.anggota.data
        f = form.urifile.data
        # cek apakah file yang diupload sesuai daftar jenis file yg diijinkan
        if (
            "." in f.filename
            and f.filename.rsplit(".", 1)[1] in app.config["ACCEPTED_FILE_TYPES"]
        ):
            filename = secure_filename(f.filename)
            origext = os.path.splitext(filename)[1]
            rndfilename = (
                "".join(random.choices(string.ascii_letters + string.digits, k=32))
                + origext
            )
            # kode baru untuk cek local file names
            cekprop = ProposalPkm.query.all()
            for item in cekprop:
                if rndfilename == item.filename:
                    rndfilename = (
                        "".join(
                            random.choices(string.ascii_letters + string.digits, k=32)
                        )
                        + origext
                    )
                    break
                else:
                    pass
            f.save(os.path.join(basedir, "uploads", rndfilename))
            path = os.path.join(basedir, "uploads", rndfilename)
            currentproposal.filename = rndfilename
            currentproposal.setQRcode(path)
            print(path)
            db.session.commit()
            return redirect(url_for("lstpkm"))
        else:
            flash(
                "File (%s) is not an accepted format. Please upload .docx or .pdf files instead"
                % f.filename
            )
            print(f.filename)
    if request.method == "GET":
        form.judul.data = currentproposal.judul
        form.biaya.data = currentproposal.biaya
        form.tahun.data = currentproposal.tahun
        form.anggota.data = currentproposal.anggota
    return render_template("updatepkm.html", title="Update proposal PkM", form=form)


## Delete prop PkM (todo)
@app.route("/dashboard/deletepkm/<id>")
@login_required
def deletepkm(id):
    selectedprop = ProposalPkm.query.filter_by(idproposalpkm=id).first()
    db.session.delete(selectedprop)
    db.session.commit()
    return redirect(url_for("lstpkm"))


# PENGAJUAN PROPOSAL PEMBIAYAAN PUBLIKASI & HKI
## Daftar pembiayaan pub HKI
@app.route("/dashboard/daftarpubhki", methods=["GET", "POST"])
@login_required
def daftarpubhki():
    form = AddPropPubHKI()
    if request.method == "POST":
        form.validate_on_submit()
        proposal = PublikasiHKI(
            judul=form.judul.data,
            biaya=form.biaya.data,
            keterangan=form.keterangan.data,
            fk_pengusul_id=current_user.idusers,
        )
        f = form.urifilebukti.data
        # cek apakah file yang diupload sesuai daftar jenis file yg diijinkan
        if (
            "." in f.filename
            and f.filename.rsplit(".", 1)[1] in app.config["ACCEPTED_FILE_TYPES"]
        ):
            filename = secure_filename(f.filename)
            origext = os.path.splitext(filename)[1]
            rndfilename = (
                "".join(random.choices(string.ascii_letters + string.digits, k=32))
                + origext
            )
            # kode baru untuk cek local file names
            cekprop = PublikasiHKI.query.all()
            for item in cekprop:
                if rndfilename == item.filename:
                    rndfilename = (
                        "".join(
                            random.choices(string.ascii_letters + string.digits, k=32)
                        )
                        + origext
                    )
                    break
                else:
                    pass
            f.save(os.path.join(basedir, "uploads", rndfilename))
            path = os.path.join(basedir, "uploads", rndfilename)
            proposal.filename = rndfilename
            proposal.setQRcode(path)
            print(path)
            db.session.add(proposal)
            db.session.commit()
            return redirect(url_for("lstpublikasihki"))
        else:
            flash(
                "File (%s) is not an accepted format. Please upload .docx or .pdf files instead"
                % f.filename
            )
            print(f.filename)
    return render_template(
        "daftarpubhki.html", title="Pengajuan pembiayaan publikasi & HKI", form=form
    )


## List Pub HKI
@app.route("/dashboard/lstpubhki")
@login_required
def lstpublikasihki():
    # page = request.args.get("page", 1, type=int)
    # pubhki = PublikasiHKI.query.paginate(page, app.config["POSTS_PER_PAGE"], False)
    pubhki = PublikasiHKI.query.all()
    # next_url = (
    #     url_for("lstpublikasihki", page=pubhki.next_num) if pubhki.has_next else None
    # )
    # prev_url = (
    #     url_for("lstpublikasihki", page=pubhki.prev_num) if pubhki.has_prev else None
    # )
    return render_template(
        "lstpublikasihki.html",
        title="Daftar pengajuan pembiayaan publikasi jurnal dan HKI",
        pubhki=pubhki,
        # pubhki=pubhki.items,
        # next_url=next_url,
        # prev_url=prev_url,
    )


## Update/edit pengajuan Pub HKI
@app.route("/dashboard/updatepubhki/<id>", methods=["GET", "POST"])
@login_required
def updatepubhki(id):
    form = UpdatePropPubHKI()
    currentproposal = PublikasiHKI.query.filter_by(idpublikasihki=id).first()
    if form.validate_on_submit():
        currentproposal.judul = form.judul.data
        currentproposal.biaya = form.biaya.data
        currentproposal.keterangan = form.keterangan.data
        f = form.urifilebukti.data
        # cek apakah file yang diupload sesuai daftar jenis file yg diijinkan
        if (
            "." in f.filename
            and f.filename.rsplit(".", 1)[1] in app.config["ACCEPTED_FILE_TYPES"]
        ):
            filename = secure_filename(f.filename)
            origext = os.path.splitext(filename)[1]
            rndfilename = (
                "".join(random.choices(string.ascii_letters + string.digits, k=32))
                + origext
            )
            # kode baru untuk cek local file names
            cekprop = PublikasiHKI.query.all()
            for item in cekprop:
                if rndfilename == item.filename:
                    rndfilename = (
                        "".join(
                            random.choices(string.ascii_letters + string.digits, k=32)
                        )
                        + origext
                    )
                    break
                else:
                    pass
            f.save(os.path.join(basedir, "uploads", rndfilename))
            path = os.path.join(basedir, "uploads", rndfilename)
            currentproposal.filename = rndfilename
            currentproposal.setQRcode(path)
            print(path)
            db.session.commit()
            return redirect(url_for("lstpublikasihki"))
        else:
            flash(
                "File (%s) is not an accepted format. Please upload .docx or .pdf files instead"
                % f.filename
            )
            print(f.filename)
    if request.method == "GET":
        form.judul.data = currentproposal.judul
        form.biaya.data = currentproposal.biaya
        form.keterangan.data = currentproposal.keterangan
    return render_template(
        "updatepubhki.html", title="Update pengajuan publikasi dan HKI", form=form
    )


## Delete pengajuan Pub HKI
@app.route("/dashboard/deletepubhki/<id>")
@login_required
def deletepubhki(id):
    selectedprop = PublikasiHKI.query.filter_by(idpublikasihki=id).first()
    db.session.delete(selectedprop)
    db.session.commit()
    return redirect(url_for("lstpublikasihki"))


# GENERATE TANDA TERIMA DSB
## Buat tanda terima penelitian
@app.route("/dashboard/tandaterima/<id>")
@login_required
def tandaterima(id):
    # buat tanda terima dalam bentuk html saja (simplest) biar user yg save as pdf/print
    selectedprop = ProposalLit.query.filter_by(idproposallit=id).first()
    tanggal = datetime.utcnow().strftime("%B %d %Y")
    return render_template(
        "sub/__tandaterima.html",
        title="Tanda Terima",
        proposal=selectedprop,
        tanggal=tanggal,
    )


## Buat tanda terima pkm
@app.route("/dashboard/tandaterimapkm/<id>")
@login_required
def tandaterimapkm(id):
    # buat tanda terima dalam bentuk html saja (simplest) biar user yg save as pdf/print
    selectedprop = ProposalPkm.query.filter_by(idproposalpkm=id).first()
    tanggal = datetime.utcnow().strftime("%B %d %Y")
    return render_template(
        "sub/__tandaterima.html",
        title="Tanda Terima",
        proposal=selectedprop,
        tanggal=tanggal,
    )


## Buat tanda terima hki
@app.route("/dashboard/tandaterimapubhki/<id>")
@login_required
def tandaterimapubhki(id):
    # buat tanda terima dalam bentuk html saja (simplest) biar user yg save as pdf/print
    selectedprop = PublikasiHKI.query.filter_by(idpublikasihki=id).first()
    tanggal = datetime.utcnow().strftime("%B %d %Y")
    return render_template(
        "sub/__tandaterima.html",
        title="Tanda Terima",
        proposal=selectedprop,
        tanggal=tanggal,
    )


## Download file proposal/panduan/templates
@app.route("/dashboard/get/<filename>", methods=["GET"])
@login_required
def get_file(filename):
    """Download a file."""
    path = os.path.join(app.config["UPLOAD_DIRECTORY"], filename)
    print(path)
    return send_file(path, as_attachment=True)


## Download log file for review
@app.route("/dashboard/dl/<logfile>", methods=["GET"])
@login_required
def dl_log_files(logfile):
    """Get the log file"""
    logpath = os.path.join(app.config["LOG_DIRECTORY"], logfile)
    print(logpath)
    return send_file(logpath, as_attachment=True)


## Export tabel penelitian as table excel
@app.route("/dashboard/get/tablelit")
@login_required
def get_excel_lit():
    """Download an excel file from html table."""
    ## join table proposallit dan users
    joinquery = (
        ProposalLit.query.join(Users)
        .with_entities(
            ProposalLit.idproposallit,
            ProposalLit.judul,
            ProposalLit.biaya,
            ProposalLit.tahun,
            Users.name,
            ProposalLit.anggota,
            ProposalLit.kategori,
            ProposalLit.tema,
        )
        .all()
    )
    print(joinquery)
    # querylit = ProposalLit.query.with_entities(
    #     ProposalLit.idproposallit,
    #     ProposalLit.judul,
    #     ProposalLit.biaya,
    #     ProposalLit.tahun,
    #     ProposalLit.fk_ketua_id,
    #     ProposalLit.anggota,
    #     ProposalLit.kategori,
    #     ProposalLit.tema,
    # ).all()
    colslit = [
        "idproposallit",
        "judul",
        "biaya",
        "tahun",
        # "fk_ketua_id",
        "name",
        "anggota",
        "kategori",
        "tema",
    ]
    return excel.make_response_from_query_sets(joinquery, colslit, "xlsx")


## Export tabel pkm as table excel
@app.route("/dashboard/get/tablepkm")
@login_required
def get_excel_pkm():
    """Download an excel file from html table."""
    ## join table proposallit dan users
    joinquery = (
        ProposalPkm.query.join(Users)
        .with_entities(
            ProposalPkm.idproposalpkm,
            ProposalPkm.judul,
            ProposalPkm.biaya,
            ProposalPkm.tahun,
            Users.name,
            ProposalPkm.anggota,
        )
        .all()
    )
    # querypkm = ProposalPkm.query.with_entities(
    #     ProposalPkm.idproposalpkm,
    #     ProposalPkm.judul,
    #     ProposalPkm.biaya,
    #     ProposalPkm.tahun,
    #     ProposalPkm.fk_ketua_id,
    #     ProposalPkm.anggota,
    # ).all()
    colspkm = ["idproposalpkm", "judul", "biaya", "tahun", "name", "anggota"]
    return excel.make_response_from_query_sets(joinquery, colspkm, "xlsx")


# ADMIN DASHBOARD

## Dashboard
@app.route("/admin/dashboard")
@login_required
def admdashboard():
    alllit = ProposalLit.query.all()
    allpkm = ProposalPkm.query.all()
    allpengumumanaktif = Pengumuman.query.filter_by(isactive="Ya").all()
    return render_template(
        "admin/admdashboard.html",
        title="Admin Dashboard",
        penelitian=alllit,
        pkm=allpkm,
        pengumuman=allpengumumanaktif,
    )


## Create Users
@app.route("/admin/dashboard/adduser", methods=["GET", "POST"])
@login_required
def admadduser():
    if current_user.is_authenticated and current_user.admin == "Ya":
        form = AdmAddUsers()
        if form.validate_on_submit():
            user = Users(
                username=form.username.data,
                email=form.email.data,
                name=form.name.data,
                role=form.role.data,
            )
            user.set_password(form.password.data)
            db.session.add(user)
            db.session.commit()
            flash("You are successfully adding new user!")
            return redirect(url_for("admlstusers"))
    return render_template("admin/admaddusers.html", title="Add New Users", form=form)


## List Users
@app.route("/admin/dashboard/lstusers")
@login_required
def admlstusers():
    users = Users.query.all()
    return render_template("admin/admlstusers.html", title="Manage Users", user=users)


## Edit Users
@app.route("/admin/dashboard/edituser/<id>", methods=["GET", "POST"])
@login_required
def admedituser(id):
    selecteduser = Users.query.filter_by(idusers=id).first()
    form = AdmEditUsers(selecteduser.username)
    if form.validate_on_submit():
        selecteduser.username = form.username.data
        selecteduser.email = form.email.data
        selecteduser.name = form.name.data
        selecteduser.role = form.role.data
        selecteduser.togglepenilai = form.togglepenilai.data
        selecteduser.admin = form.admin.data
        db.session.commit()
        return redirect(url_for("admlstusers"))
    if request.method == "GET":
        form.username.data = selecteduser.username
        form.email.data = selecteduser.email
        form.name.data = selecteduser.name
        form.role.data = selecteduser.role
        form.togglepenilai.data = selecteduser.togglepenilai
        form.admin.data = selecteduser.admin
    return render_template("admin/admeditusers.html", title="Edit User", form=form)


## Create Pengumuman
@app.route("/admin/dashboard/admaddpengumuman", methods=["GET", "POST"])
@login_required
def admaddpengumuman():
    form = AdmBuatPengumuman()
    if form.validate_on_submit():
        pengumuman = Pengumuman(
            judul=form.judul.data, isi=form.isi.data, isactive=form.isactive.data
        )
        if form.urifile.data:
            f = form.urifile.data
            filename = secure_filename(f.filename)
            origext = os.path.splitext(filename)[1]
            rndfilename = (
                "".join(random.choices(string.ascii_letters + string.digits, k=32))
                + origext
            )
            f.save(os.path.join(basedir, "static/img", rndfilename))
            path = os.path.join(basedir, "static/img", rndfilename)
            pengumuman.filename = rndfilename
        db.session.add(pengumuman)
        db.session.commit()
        return redirect(url_for("admlstpengumuman"))
    return render_template(
        "admin/admpengumuman.html", title="Tambah pengumuman", form=form
    )


## Edit Pengumuman
@app.route("/admin/dashboard/admeditpengumuman/<id>", methods=["GET", "POST"])
@login_required
def admeditpengumuman(id):
    form = AdmEditPengumuman()
    selectedpengumuman = Pengumuman.query.filter_by(idpengumuman=id).first()
    if form.validate_on_submit():
        selectedpengumuman.judul = form.judul.data
        selectedpengumuman.isi = form.isi.data
        if form.urifile.data:
            f = form.urifile.data
            filename = secure_filename(f.filename)
            origext = os.path.splitext(filename)[1]
            rndfilename = (
                "".join(random.choices(string.ascii_letters + string.digits, k=32))
                + origext
            )
            f.save(os.path.join(basedir, "static/img", rndfilename))
            path = os.path.join(basedir, "static/img", rndfilename)
            selectedpengumuman.filename = rndfilename
        db.session.commit()
        return redirect(url_for("admlstpengumuman"))
    if request.method == "GET":
        form.judul.data = selectedpengumuman.judul
        form.isi.data = selectedpengumuman.isi
    return render_template(
        "admin/admeditpengumuman.html", title="Update pengumuman", form=form
    )


## List Pengumuman
@app.route("/admin/dashboard/admlstpengumuman")
@login_required
def admlstpengumuman():
    pengumuman = Pengumuman.query.order_by(Pengumuman.created_at.desc()).all()
    path = ""
    for item in pengumuman:
        if item.filename:
            path = os.path.join(basedir, "uploads", item.filename)
        else:
            path = ""
    return render_template(
        "admin/admlstpengumuman.html",
        title="Daftar pengumuman",
        pengumuman=pengumuman,
        path=path,
    )


## Delete pengumuman
@app.route("/dashboard/deletepengumuman/<id>")
@login_required
def admdeletepengumuman(id):
    selectedpeng = Pengumuman.query.filter_by(idpengumuman=id).first()
    db.session.delete(selectedpeng)
    db.session.commit()
    return redirect(url_for("admlstpengumuman"))


## Upload files
@app.route("/admin/dashboard/admuploadfile", methods=["GET", "POST"])
@login_required
def admupload():
    form = AdmUploadFile()
    if form.validate_on_submit():
        if form.urifile.data:
            f = form.urifile.data
            filename = secure_filename(f.filename)
            origext = os.path.splitext(filename)[1]
            newfilename = form.namafile.data + origext
            f.save(os.path.join(basedir, "uploads", newfilename))
            path = os.path.join(basedir, "uploads", newfilename)
        db.session.commit()
        return redirect(url_for("admdashboard"))
    return render_template("admin/admuploadfile.html", title="Upload file", form=form)


## List files in upload dir
@app.route("/admin/dashboard/admlistfile", methods=["GET", "POST"])
@login_required
def admlistfile():
    allfiles = [
        f
        for f in os.listdir(os.path.join(basedir, "uploads"))
        if os.path.isfile(os.path.join(basedir, "uploads", f))
    ]
    allproplit = ProposalLit.query.all()
    allproppkm = ProposalPkm.query.all()
    allpubhki = PublikasiHKI.query.all()
    print(allfiles)
    return render_template(
        "admin/admlstfiles.html",
        title="Daftar File",
        filename=allfiles,
        proplit=allproplit,
        proppkm=allproppkm,
        pubhki=allpubhki,
    )


## List files in logs directory
@app.route("/admin/dashboard/admlistlogs", methods=["GET", "POST"])
@login_required
def admlistlogs():
    allfiles = [
        f
        for f in os.listdir(app.config["LOG_DIRECTORY"])
        if os.path.isfile(os.path.join(app.config["LOG_DIRECTORY"], f))
    ]
    print(allfiles)
    return render_template(
        "admin/admlstlogs.html",
        title="System logs",
        logname=allfiles,
    )


## Delete file from server
@app.route("/admin/dashboard/admdeletefile/<filename>", methods=["GET", "POST"])
@login_required
def admdeletefile(filename):
    if os.path.exists(os.path.join(basedir, "uploads", filename)):
        os.remove(os.path.join(basedir, "uploads", filename))
        flash("File successfully deleted.")
    else:
        flash("File not found")
    return redirect(url_for("admlistfile"))


# PENILAI DASHBOARD

## Dashboard
@app.route("/penilai/dashboard")
@login_required
def pendashboard():
    alllit = ProposalLit.query.all()
    allpkm = ProposalPkm.query.all()
    allpengumumanaktif = Pengumuman.query.filter_by(isactive="Ya").all()
    allnilai = []
    # allnilailitdsr = NilaiDasar.query.all()
    # allnilailitterap = NilaiTerapan.query.all()
    # allnilaipkm = NilaiPkm.query.all()
    allskorlitdasar = SkorPropDasar.query.all()
    allskorlitterap = SkorPropTerapan.query.all()
    allskorlitbang = SkorPropPengembangan.query.all()
    allskorpkm = SkorPropPkm.query.all()
    for item in alllit:
        if item.kategori == "Dasar":
            for n in allskorlitdasar:
                if n.fk_proposal_id == item.idproposallit:
                    allnilai.append(n)
        elif item.kategori == "Terapan":
            for n in allskorlitterap:
                if n.fk_proposal_id == item.idproposallit:
                    allnilai.append(n)
        elif item.kategori == "Pengembangan":
            for n in allskorlitbang:
                if n.fk_proposal_id == item.idproposallit:
                    allnilai.append(n)
    return render_template(
        "penilai/penilaidashboard.html",
        title="Dashboard Penilai",
        penelitian=alllit,
        pkm=allpkm,
        pengumuman=allpengumumanaktif,
        nilai=allnilai,
    )


## List pemilihan proposal lit dasar yg akan dinilai
@app.route("/penilai/penilaianlitdasar")
@login_required
def penilaianlitdasar():
    allnilai = []
    # allnilailitdsr = NilaiDasar.query.all()
    allskorlitdasar = SkorPropDasar.query.all()
    alllitdasar = ProposalLit.query.filter_by(kategori="Dasar").all()
    for item in alllitdasar:
        for n in allskorlitdasar:
            if n.fk_proposal_id == item.idproposallit:
                allnilai.append(n)
    return render_template(
        "penilai/penilaianlitdasar.html",
        title="Halaman Penilaian Penelitian Dasar",
        penelitian=alllitdasar,
        nilai=allnilai,
    )


## Input nilai prop lit dasar
@app.route("/penilai/nilaiproplitdasar/<id>", methods=["GET", "POST"])
@login_required
def nilaiproplitdasar(id):
    # form = PenBuatNilaiLitDasar()
    form = SkorPropDasarForm()
    selectedprop = ProposalLit.query.filter_by(idproposallit=id).first()
    path = os.path.join(basedir, "uploads", selectedprop.filename)
    print(path)
    if form.validate_on_submit():
        # nilai = NilaiDasar(nrumusanmslh=form.nrumusanmslh.data, nluaran=form.nluaran.data, npustaka=form.npustaka.data, nmetode=form.nmetode.data, nkelayakan=form.nkelayakan.data, catatan=form.catatan.data, fk_proposal_id=selectedprop.idproposallit)
        skor = SkorPropDasar(
            skorrelevansirip=form.skorrelevansirip.data,
            skormetode=form.skormetode.data,
            skorluaran=form.skorluaran.data,
            skorpustaka=form.skorpustaka.data,
            skorkelayakan=form.skorkelayakan.data,
            skorketerlibatanmhs=form.skorketerlibatanmhs.data,
            fk_proposal_id=selectedprop.idproposallit,
        )
        total = (
            (20 * int(form.skorrelevansirip.data))
            + (20 * int(form.skormetode.data))
            + (30 * int(form.skorluaran.data))
            + (10 * int(form.skorpustaka.data))
            + (15 * int(form.skorkelayakan.data))
            + (5 * int(form.skorketerlibatanmhs.data))
        )
        print(form.skorrelevansirip.data)
        print(total)
        skor.nilaitotal = total
        db.session.add(skor)
        db.session.commit()
        return redirect(url_for("penilaianlitdasar"))
    return render_template(
        "penilai/nilaiproplitdasar.html",
        title="Input penilaian Penelitian Dasar",
        proposal=selectedprop,
        path=path,
        form=form,
    )


## Update/edit nilai prop lit dasar
@app.route("/penilai/editnilaidasar/<id>", methods=["GET", "POST"])
@login_required
def editnilaidasar(id):
    form = EditSkorPropDasarForm()
    selectedprop = ProposalLit.query.filter_by(idproposallit=id).first()
    # n = NilaiDasar.query.filter_by(fk_proposal_id=id).first()
    n = SkorPropDasar.query.filter_by(fk_proposal_id=id).first()
    if form.validate_on_submit():
        n.skorrelevansirip = form.skorrelevansirip.data
        n.skormetode = form.skormetode.data
        n.skorluaran = form.skorluaran.data
        n.skorpustaka = form.skorpustaka.data
        n.skorkelayakan = form.skorkelayakan.data
        n.skorketerlibatanmhs = form.skorketerlibatanmhs.data
        n.catatan = form.catatan.data
        # total = (0.25 * float(form.nrumusanmslh.data)) + (0.25 * float(form.nluaran.data)) + (0.15 * float(form.npustaka.data)) + (0.25 * float(form.nmetode.data)) + (0.1 * float(form.nkelayakan.data))
        total = (
            (20 * int(form.skorrelevansirip.data))
            + (20 * int(form.skormetode.data))
            + (30 * int(form.skorluaran.data))
            + (10 * int(form.skorpustaka.data))
            + (15 * int(form.skorkelayakan.data))
            + (5 * int(form.skorketerlibatanmhs.data))
        )
        n.nilaitotal = total
        db.session.commit()
        return redirect(url_for("penilaianlitdasar"))
    if request.method == "GET":
        form.skorrelevansirip.data = n.skorrelevansirip
        form.skormetode.data = n.skormetode
        form.skorluaran.data = n.skorluaran
        form.skorpustaka.data = n.skorpustaka
        form.skorkelayakan.data = n.skorkelayakan
        form.skorketerlibatanmhs.data = n.skorketerlibatanmhs
        form.catatan.data = n.catatan
    return render_template(
        "penilai/updatenilaiproplitdasar.html",
        title="Update nilai Penelitian Dasar",
        form=form,
        proposal=selectedprop,
    )


## List pemilihan proposal lit terapan yg akan dinilai
@app.route("/penilai/penilaianlitterap")
@login_required
def penilaianlitterap():
    allnilai = []
    # allnilailittrp = NilaiTerapan.query.all()
    allskorlitterap = SkorPropTerapan.query.all()
    alllitterap = ProposalLit.query.filter_by(kategori="Terapan").all()
    for item in alllitterap:
        for n in allskorlitterap:
            if n.fk_proposal_id == item.idproposallit:
                allnilai.append(n)
    return render_template(
        "penilai/penilaianlitterap.html",
        title="Halaman penilaian proposal Penelitian Terapan",
        penelitian=alllitterap,
        nilai=allnilai,
    )


## Input nilai prop lit terapan
@app.route("/penilai/nilaiproplitterap/<id>", methods=["GET", "POST"])
@login_required
def nilaiproplitterap(id):
    # form = PenBuatNilaiLitTerap()
    form = SkorPropTerapanForm()
    selectedprop = ProposalLit.query.filter_by(idproposallit=id).first()
    path = os.path.join(basedir, "uploads", selectedprop.filename)
    print(path)
    if form.validate_on_submit():
        # nilai = NilaiTerapan(nroadmap=form.nroadmap.data, nluaran=form.nluaran.data, npustaka=form.npustaka.data, nmetode=form.nmetode.data, nkelayakan=form.nkelayakan.data, catatan=form.catatan.data, fk_proposal_id=selectedprop.idproposallit)
        skor = SkorPropTerapan(
            skorrelevansirip=form.skorrelevansirip.data,
            skormetode=form.skormetode.data,
            skorluaran=form.skorluaran.data,
            skorpustaka=form.skorpustaka.data,
            skorkelayakan=form.skorkelayakan.data,
            skormitra=form.skormitra.data,
            skorketerlibatanmhs=form.skorketerlibatanmhs.data,
            fk_proposal_id=selectedprop.idproposallit,
        )
        # total = (0.15 * float(form.nroadmap.data)) + (0.35 * float(form.nluaran.data)) + (0.15 * float(form.npustaka.data)) + (0.20 * float(form.nmetode.data)) + (0.15 * float(form.nkelayakan.data))
        total = (
            (25 * int(form.skorrelevansirip.data))
            + (15 * int(form.skormetode.data))
            + (20 * int(form.skorluaran.data))
            + (10 * int(form.skorpustaka.data))
            + (15 * int(form.skorkelayakan.data))
            + (10 * int(form.skormitra.data))
            + (5 * int(form.skorketerlibatanmhs.data))
        )
        print(total)
        skor.nilaitotal = total
        db.session.add(skor)
        db.session.commit()
        return redirect(url_for("penilaianlitterap"))
    return render_template(
        "penilai/nilaiproplitterap.html",
        title="Input penilaian proposal Penelitian Terapan",
        proposal=selectedprop,
        path=path,
        form=form,
    )


## Update/edit nilai prop lit terap
@app.route("/penilai/editnilaiterap/<id>", methods=["GET", "POST"])
@login_required
def editnilaiterap(id):
    # form = EditNilaiLitTerap()
    form = EditSkorPropTerapanForm()
    selectedprop = ProposalLit.query.filter_by(idproposallit=id).first()
    # n = NilaiTerapan.query.filter_by(fk_proposal_id=id).first()
    n = SkorPropTerapan.query.filter_by(fk_proposal_id=id).first()
    if form.validate_on_submit():
        n.skorrelevansirip = form.skorrelevansirip.data
        n.skormetode = form.skormetode.data
        n.skorluaran = form.skorluaran.data
        n.skorpustaka = form.skorpustaka.data
        n.skorkelayakan = form.skorkelayakan.data
        n.skormitra = form.skormitra.data
        n.skorketerlibatanmhs = form.skorketerlibatanmhs.data
        n.catatan = form.catatan.data
        # total = (0.25 * float(form.nroadmap.data)) + (0.25 * float(form.nluaran.data)) + (0.15 * float(form.npustaka.data)) + (0.25 * float(form.nmetode.data)) + (0.1 * float(form.nkelayakan.data))
        total = (
            (25 * int(form.skorrelevansirip.data))
            + (15 * int(form.skormetode.data))
            + (20 * int(form.skorluaran.data))
            + (10 * int(form.skorpustaka.data))
            + (15 * int(form.skorkelayakan.data))
            + (10 * int(form.skormitra.data))
            + (5 * int(form.skorketerlibatanmhs.data))
        )
        n.nilaitotal = total
        db.session.commit()
        return redirect(url_for("penilaianlitterap"))
    if request.method == "GET":
        form.skorrelevansirip.data = n.skorrelevansirip
        form.skormetode.data = n.skormetode
        form.skorluaran.data = n.skorluaran
        form.skorpustaka.data = n.skorpustaka
        form.skorkelayakan.data = n.skorkelayakan
        form.skormitra.data = n.skormitra
        form.skorketerlibatanmhs.data = n.skorketerlibatanmhs
        form.catatan.data = n.catatan
    return render_template(
        "penilai/updatenilaiproplitterap.html",
        title="Update nilai proposal Penelitian Terapan",
        form=form,
        proposal=selectedprop,
    )


## List pemilihan proposal lit pengembangan yg akan dinilai
@app.route("/penilai/penilaianlitbang")
@login_required
def penilaianlitbang():
    allnilai = []
    allskorlitbang = SkorPropPengembangan.query.all()
    alllitbang = ProposalLit.query.filter_by(kategori="Pengembangan").all()
    for item in alllitbang:
        for n in allskorlitbang:
            if n.fk_proposal_id == item.idproposallit:
                allnilai.append(n)
    return render_template(
        "penilai/penilaianlitterap.html",
        title="Halaman penilaian proposal Penelitian Pengembangan",
        penelitian=alllitbang,
        nilai=allnilai,
    )


## Input nilai prop lit pengembangan
@app.route("/penilai/nilaiproplitbang/<id>", methods=["GET", "POST"])
@login_required
def nilaiproplitbang(id):
    form = SkorPropPengembanganForm()
    selectedprop = ProposalLit.query.filter_by(idproposallit=id).first()
    path = os.path.join(basedir, "uploads", selectedprop.filename)
    print(path)
    if form.validate_on_submit():
        skor = SkorPropPengembangan(
            skorrelevansirip=form.skorrelevansirip.data,
            skormetode=form.skormetode.data,
            skorluaran=form.skorluaran.data,
            skorpustaka=form.skorpustaka.data,
            skorkelayakan=form.skorkelayakan.data,
            skormitra=form.skormitra.data,
            skorketerlibatanmhs=form.skorketerlibatanmhs.data,
            fk_proposal_id=selectedprop.idproposallit,
        )
        total = (
            (20 * int(form.skorrelevansirip.data))
            + (15 * int(form.skormetode.data))
            + (20 * int(form.skorluaran.data))
            + (10 * int(form.skorpustaka.data))
            + (20 * int(form.skorkelayakan.data))
            + (10 * int(form.skormitra.data))
            + (5 * int(form.skorketerlibatanmhs.data))
        )
        print(total)
        skor.nilaitotal = total
        db.session.add(skor)
        db.session.commit()
        return redirect(url_for("penilaianlitbang"))
    return render_template(
        "penilai/nilaiproplitbang.html",
        title="Input penilaian proposal Penelitian Pengembangan",
        proposal=selectedprop,
        path=path,
        form=form,
    )


## Update/edit nilai prop lit pengembangan
@app.route("/penilai/editnilaibang/<id>", methods=["GET", "POST"])
@login_required
def editnilaibang(id):
    form = EditSkorPropPengembanganForm()
    selectedprop = ProposalLit.query.filter_by(idproposallit=id).first()
    n = SkorPropPengembangan.query.filter_by(fk_proposal_id=id).first()
    if form.validate_on_submit():
        n.skorrelevansirip = form.skorrelevansirip.data
        n.skormetode = form.skormetode.data
        n.skorluaran = form.skorluaran.data
        n.skorpustaka = form.skorpustaka.data
        n.skorkelayakan = form.skorkelayakan.data
        n.skormitra = form.skormitra.data
        n.skorketerlibatanmhs = form.skorketerlibatanmhs.data
        n.catatan = form.catatan.data
        total = (
            (20 * int(form.skorrelevansirip.data))
            + (15 * int(form.skormetode.data))
            + (20 * int(form.skorluaran.data))
            + (10 * int(form.skorpustaka.data))
            + (20 * int(form.skorkelayakan.data))
            + (10 * int(form.skormitra.data))
            + (5 * int(form.skorketerlibatanmhs.data))
        )
        n.nilaitotal = total
        db.session.commit()
        return redirect(url_for("penilaianlitbang"))
    if request.method == "GET":
        form.skorrelevansirip.data = n.skorrelevansirip
        form.skormetode.data = n.skormetode
        form.skorluaran.data = n.skorluaran
        form.skorpustaka.data = n.skorpustaka
        form.skorkelayakan.data = n.skorkelayakan
        form.skormitra.data = n.skormitra
        form.skorketerlibatanmhs.data = n.skorketerlibatanmhs
        form.catatan.data = n.catatan
    return render_template(
        "penilai/updatenilaiproplitbang.html",
        title="Update nilai proposal Penelitian Pengembangan",
        form=form,
        proposal=selectedprop,
    )


## List pemilihan proposal PkM yg akan dinilai
@app.route("/penilai/penilaianpkm")
@login_required
def penilaianpkm():
    allnilai = []
    # allnilaipkm = NilaiPkm.query.all()
    allskorpkm = SkorPropPkm.query.all()
    allpkm = ProposalPkm.query.all()
    for item in allpkm:
        for n in allskorpkm:
            if n.fk_proposal_id == item.idproposalpkm:
                allnilai.append(n)
    return render_template(
        "penilai/penilaianpkm.html",
        title="Halaman penilaian proposal PkM",
        penelitian=allpkm,
        nilai=allnilai,
    )


## Input nilai prop PkM
@app.route("/penilai/nilaiproppkm/<id>", methods=["GET", "POST"])
@login_required
def nilaiproppkm(id):
    # form = PenBuatNilaiPkm()
    form = SkorPropPkmForm()
    selectedprop = ProposalPkm.query.filter_by(idproposalpkm=id).first()
    path = os.path.join(basedir, "uploads", selectedprop.filename)
    # n = NilaiPkm.query.filter_by(fk_proposal_id=id).first()
    print(path)
    if form.validate_on_submit():
        skor = SkorPropPkm(
            skorsituasi=form.skorsituasi.data,
            skormitra=form.skormitra.data,
            skorsolusi=form.skorsolusi.data,
            skorluaran=form.skorluaran.data,
            skorkelayakan=form.skorkelayakan.data,
            catatan=form.catatan.data,
            skorbiaya=form.skorbiaya.data,
            fk_proposal_id=selectedprop.idproposalpkm,
        )
        # total = (0.2 * float(form.nsituasi.data)) + (0.15 * float(form.nmitra.data)) + (0.2 * float(form.nsolusi.data)) + (0.15 * float(form.nluaran.data)) + (0.1 * float(form.nkelayakan.data)) + (0.2 * float(form.nbiaya.data))
        total = (
            (20 * int(form.skorsituasi.data))
            + (15 * int(form.skormitra.data))
            + (20 * int(form.skorsolusi.data))
            + (15 * int(form.skorluaran.data))
            + (10 * int(form.skorkelayakan.data))
            + (20 * int(form.skorbiaya.data))
        )
        print(total)
        skor.nilaitotal = total
        db.session.add(skor)
        db.session.commit()
        return redirect(url_for("penilaianpkm"))
    return render_template(
        "penilai/nilaiproppkm.html",
        title="Input penilaian proposal Pengabdian Kepada Masyarakat",
        proposal=selectedprop,
        path=path,
        form=form,
    )


## Update/edit nilai proposal PkM
@app.route("/penilai/editnilaipkm/<id>", methods=["GET", "POST"])
@login_required
def editnilaipkm(id):
    # form = EditNilaiPkm()
    form = EditSkorPropPkmForm()
    selectedprop = ProposalPkm.query.filter_by(idproposalpkm=id).first()
    # n = NilaiPkm.query.filter_by(fk_proposal_id=id).first()
    n = SkorPropPkm.query.filter_by(fk_proposal_id=id).first()
    if form.validate_on_submit():
        n.skorsituasi = form.skorsituasi.data
        n.skormitra = form.skormitra.data
        n.skorsolusi = form.skorsolusi.data
        n.skorluaran = form.skorluaran.data
        n.skorkelayakan = form.skorkelayakan.data
        n.skorbiaya = form.skorbiaya.data
        n.catatan = form.catatan.data
        total = (
            (20 * int(form.skorsituasi.data))
            + (15 * int(form.skormitra.data))
            + (20 * int(form.skorsolusi.data))
            + (15 * int(form.skorluaran.data))
            + (10 * int(form.skorkelayakan.data))
            + (20 * int(form.skorbiaya.data))
        )
        n.nilaitotal = total
        db.session.commit()
        return redirect(url_for("penilaianpkm"))
    if request.method == "GET":
        form.skorsituasi.data = n.skorsituasi
        form.skormitra.data = n.skormitra
        form.skorsolusi.data = n.skorsolusi
        form.skorluaran.data = n.skorluaran
        form.skorkelayakan.data = n.skorkelayakan
        form.skorbiaya.data = n.skorbiaya
        form.catatan.data = n.catatan
    return render_template(
        "penilai/updatenilaiproppkm.html",
        title="Update nilai proposal Pengabdian Kepada Masyarakat",
        form=form,
        proposal=selectedprop,
    )


## Set status proposal penelitian
@app.route("/penilai/setstatusproplit/<id>", methods=["GET", "POST"])
@login_required
def setstatusproplit(id):
    form = SetStatusProp()
    selectedproplit = ProposalLit.query.filter_by(idproposallit=id).first()
    if form.validate_on_submit():
        selectedproplit.status = form.propstatus.data
        db.session.commit()
        return redirect(url_for("pendashboard"))
    return render_template(
        "penilai/editstatusprop.html",
        title="Set status proposal",
        proposal=selectedproplit,
        form=form,
    )


## Set status proposal PkM
@app.route("/penilai/setstatusproppkm/<id>", methods=["GET", "POST"])
@login_required
def setstatusproppkm(id):
    form = SetStatusProp()
    selectedproppkm = ProposalPkm.query.filter_by(idproposalpkm=id).first()
    if form.validate_on_submit():
        selectedproppkm.status = form.propstatus.data
        db.session.commit()
        return redirect(url_for("pendashboard"))
    return render_template(
        "penilai/editstatusprop.html",
        title="Set status proposal",
        proposal=selectedproppkm,
        form=form,
    )


## User cek catatan penilaian prop penelitian
@app.route("/dashboard/catatanpenilaianlit/<id>")
def catatanpenilaianlit(id):
    selectedprop = ProposalLit.query.filter_by(idproposallit=id).first()
    catatannilai = ""
    # selectedndasar = NilaiDasar.query.filter_by(fk_proposal_id=id).first()
    # selectednterap = NilaiTerapan.query.filter_by(fk_proposal_id=id).first()
    selectedndasar = SkorPropDasar.query.filter_by(fk_proposal_id=id).first()
    selectednterap = SkorPropTerapan.query.filter_by(fk_proposal_id=id).first()
    selectednbang = SkorPropPengembangan.query.filter_by(fk_proposal_id=id).first()
    if selectedprop.kategori == "Dasar":
        catatannilai = selectedndasar.catatan
        print(catatannilai)
    elif selectedprop.kategori == "Terapan":
        catatannilai = selectednterap.catatan
        print(catatannilai)
    elif selectedprop.kategori == "Pengembangan":
        catatannilai = selectednbang.catatan
        print(catatannilai)
    return render_template(
        "sub/__catatan.html",
        title="Catatan penilaian proposal",
        proposal=selectedprop,
        catatan=catatannilai,
    )


## User cek catatan penilaian prop pkm
@app.route("/dashboard/catatanpenilaianpkm/<id>")
def catatanpenilaianpkm(id):
    selectedprop = ProposalPkm.query.filter_by(idproposalpkm=id).first()
    catatannilai = ""
    # selectednilai = NilaiPkm.query.filter_by(fk_proposal_id=id).first()
    selectednilai = SkorPropPkm.query.filter_by(fk_proposal_id=id).first()
    catatannilai = selectednilai.catatan
    return render_template(
        "sub/__catatan.html",
        title="Catatan penilaian proposal",
        proposal=selectedprop,
        catatan=catatannilai,
    )
