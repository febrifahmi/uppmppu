from webapp import db, login
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from datetime import datetime
from hashlib import md5


class Users(UserMixin, db.Model):
    __tablename__ = "users"
    idusers = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    name = db.Column(db.String(96))
    password_hash = db.Column(db.String(128))
    role = db.Column(db.String(45))
    togglepenilai = db.Column(db.String(5), nullable=False, default="Tidak")
    admin = db.Column(db.String(5), nullable=False, default="Tidak")
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.now)
    updated_at = db.Column(db.DateTime, onupdate=datetime.now)

    # fk

    # relationship
    proposalpenelitian = db.relationship("ProposalLit", backref="ketua", lazy=True)
    proposalpkm = db.relationship("ProposalPkm", backref="ketua", lazy=True)
    proppubhki = db.relationship("PublikasiHKI", backref="pengusul", lazy=True)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def avatar(self, size):
        digest = md5(self.email.lower().encode("utf-8")).hexdigest()
        return "https://www.gravatar.com/avatar/{}?d=identicon&s={}".format(
            digest, size
        )

    def get_id(self):
        return self.idusers

    def __repr__(self):
        return "<User {}>".format(self.username)


@login.user_loader
def load_user(id):
    return Users.query.get(int(id))


class ProposalLit(db.Model):
    __tablename__ = "proposallit"
    idproposallit = db.Column(db.Integer, primary_key=True)
    judul = db.Column(db.String(1000), nullable=False)
    biaya = db.Column(db.Integer)
    tahun = db.Column(db.Integer)
    anggota = db.Column(db.String(1000))
    kategori = db.Column(db.String(45))
    tema = db.Column(db.String(45))
    koderumpun = db.Column(db.Integer)
    namarumpun = db.Column(db.String(45))
    qrcodestring = db.Column(db.String(128))
    urifile = db.Column(db.String(512))
    filename = db.Column(db.String(128))
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.now)
    updated_at = db.Column(db.DateTime, onupdate=datetime.now)
    status = db.Column(db.String(45), default="Belum dinilai")
    dinilai = db.Column(db.String(45))

    # fk
    fk_ketua_id = db.Column(db.Integer, db.ForeignKey("users.idusers"))

    # relationship
    skordasar = db.relationship("SkorPropDasar", backref="proposal", lazy=True)
    skorterapan = db.relationship("SkorPropTerapan", backref="proposal", lazy=True)
    skorpengembangan = db.relationship(
        "SkorPropPengembangan", backref="proposal", lazy=True
    )

    def setQRcode(self, path):
        self.qrcodestring = "{}".format(path)
        # self.qrcodestring = '{}-v{}'.format(self.judul, self.created_at)

    def __repr__(self):
        return "<ProposalLit {}>".format(self.judul)


class ProposalPkm(db.Model):
    __tablename__ = "proposalpkm"
    idproposalpkm = db.Column(db.Integer, primary_key=True)
    judul = db.Column(db.String(1000), nullable=False)
    biaya = db.Column(db.Integer)
    tahun = db.Column(db.Integer)
    anggota = db.Column(db.String(1000))
    qrcodestring = db.Column(db.String(128))
    urifile = db.Column(db.String(512))
    filename = db.Column(db.String(128))
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.now)
    updated_at = db.Column(db.DateTime, onupdate=datetime.now)
    status = db.Column(db.String(45), default="Belum dinilai")
    dinilai = db.Column(db.String(45))

    # fk
    fk_ketua_id = db.Column(db.Integer, db.ForeignKey("users.idusers"))

    # relationship
    skorpkm = db.relationship("SkorPropPkm", backref="proposal", lazy=True)

    def setQRcode(self, path):
        self.qrcodestring = "{}".format(path)
        # self.qrcodestring = '{}-v{}'.format(self.judul, self.created_at)

    def __repr__(self):
        return "<ProposalPkm {}>".format(self.judul)


class PublikasiHKI(db.Model):
    __tablename__ = "publikasihki"
    idpublikasihki = db.Column(db.Integer, primary_key=True)
    judul = db.Column(db.String(1000), nullable=False)
    biaya = db.Column(db.Integer)
    keterangan = db.Column(db.String(1000))
    urifilebukti = db.Column(db.String(512))
    filename = db.Column(db.String(128))
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.now)
    updated_at = db.Column(db.DateTime, onupdate=datetime.now)
    status = db.Column(db.String(45), default="Belum dinilai")
    dinilai = db.Column(db.String(45))

    # fk
    fk_pengusul_id = db.Column(db.Integer, db.ForeignKey("users.idusers"))

    def setQRcode(self, path):
        self.qrcodestring = "{}".format(path)
        # self.qrcodestring = '{}-v{}'.format(self.judul, self.created_at)

    def __repr__(self):
        return "<ProposalPubHKI {}>".format(self.judul)


class Pengumuman(db.Model):
    __tablename__ = "pengumuman"
    idpengumuman = db.Column(db.Integer, primary_key=True)
    judul = db.Column(db.String(256), nullable=False)
    isi = db.Column(db.String(1000), nullable=False)
    urifile = db.Column(db.String(512))
    filename = db.Column(db.String(128))
    isactive = db.Column(db.String(3))
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.now)
    updated_at = db.Column(db.DateTime, onupdate=datetime.now)

    # fk
    fk_author_id = db.Column(db.Integer, db.ForeignKey("users.idusers"))

    def __repr__(self):
        return "<Pengumuman {}>".format(self.judul)


# REVISI MODEL SKORING


class SkorPropDasar(db.Model):
    __tablename__ = "skorpropdasar"
    idskorpropdasar = db.Column(db.Integer, primary_key=True)
    skorrelevansirip = db.Column(db.Integer)
    skormetode = db.Column(db.Integer)
    skorluaran = db.Column(db.Integer)
    skorpustaka = db.Column(db.Integer)
    skorkelayakan = db.Column(db.Integer)
    skorketerlibatanmhs = db.Column(db.Integer)
    catatan = db.Column(db.String(1000))
    nilaitotal = db.Column(db.Integer)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.now)
    updated_at = db.Column(db.DateTime, onupdate=datetime.now)

    # fk
    fk_proposal_id = db.Column(db.Integer, db.ForeignKey("proposallit.idproposallit"))

    def __repr__(self):
        return "<NilaiPropDasar {}>".format(self.nilaitotal)


class SkorPropTerapan(db.Model):
    __tablename__ = "skorpropterapan"
    idskorpropterapan = db.Column(db.Integer, primary_key=True)
    skorrelevansirip = db.Column(db.Integer)
    skormetode = db.Column(db.Integer)
    skorluaran = db.Column(db.Integer)
    skorpustaka = db.Column(db.Integer)
    skorkelayakan = db.Column(db.Integer)
    skormitra = db.Column(db.Integer)
    skorketerlibatanmhs = db.Column(db.Integer)
    catatan = db.Column(db.String(1000))
    nilaitotal = db.Column(db.Integer)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.now)
    updated_at = db.Column(db.DateTime, onupdate=datetime.now)

    # fk
    fk_proposal_id = db.Column(db.Integer, db.ForeignKey("proposallit.idproposallit"))

    def __repr__(self):
        return "<NilaiPropTerapan {}>".format(self.nilaitotal)


class SkorPropPengembangan(db.Model):
    __tablename__ = "skorproppengembangan"
    idskorproppengembangan = db.Column(db.Integer, primary_key=True)
    skorrelevansirip = db.Column(db.Integer)
    skormetode = db.Column(db.Integer)
    skorluaran = db.Column(db.Integer)
    skorpustaka = db.Column(db.Integer)
    skorkelayakan = db.Column(db.Integer)
    skormitra = db.Column(db.Integer)
    skorketerlibatanmhs = db.Column(db.Integer)
    catatan = db.Column(db.String(1000))
    nilaitotal = db.Column(db.Integer)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.now)
    updated_at = db.Column(db.DateTime, onupdate=datetime.now)

    # fk
    fk_proposal_id = db.Column(db.Integer, db.ForeignKey("proposallit.idproposallit"))

    def __repr__(self):
        return "<NilaiPropPengembangan {}>".format(self.nilaitotal)


class SkorPropPkm(db.Model):
    __tablename__ = "skorproppkm"
    idskorproppkm = db.Column(db.Integer, primary_key=True)
    skorsituasi = db.Column(db.Integer)
    skormitra = db.Column(db.Integer)
    skorsolusi = db.Column(db.Integer)
    skorluaran = db.Column(db.Integer)
    skorkelayakan = db.Column(db.Integer)
    skorbiaya = db.Column(db.Integer)
    catatan = db.Column(db.String(1000))
    nilaitotal = db.Column(db.Integer)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.now)
    updated_at = db.Column(db.DateTime, onupdate=datetime.now)

    # fk
    fk_proposal_id = db.Column(db.Integer, db.ForeignKey("proposalpkm.idproposalpkm"))

    def __repr__(self):
        return "<NilaiPropPkm {}>".format(self.nilaitotal)
