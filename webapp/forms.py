from flask_wtf import FlaskForm, RecaptchaField
from flask_wtf.file import FileField, FileRequired
from werkzeug.utils import secure_filename
from wtforms import (
    StringField,
    PasswordField,
    BooleanField,
    SubmitField,
    SelectField,
    TextAreaField,
)
from wtforms.validators import (
    ValidationError,
    DataRequired,
    Email,
    EqualTo,
    AnyOf,
    NumberRange,
)
from webapp.models import (
    Users,
    SkorPropDasar,
    SkorPropTerapan,
    SkorPropPengembangan,
    SkorPropPkm,
)


class LoginForm(FlaskForm):
    username = StringField("Username", validators=[DataRequired()])
    password = PasswordField("Password", validators=[DataRequired()])
    remember_me = BooleanField("Remember Me")
    # recaptcha = RecaptchaField("LOGIN")
    submit = SubmitField("LOGIN")


class RegistrationForm(FlaskForm):
    username = StringField("Username", validators=[DataRequired()])
    email = StringField("Email", validators=[DataRequired(), Email()])
    nama = StringField("Nama", validators=[DataRequired()])
    posisi = SelectField(
        u"Posisi",
        choices=[("Dosen", "Dosen"), ("UPPM", "UPPM"), ("Penilai", "Penilai")],
    )
    password = PasswordField("Password", validators=[DataRequired()])
    password2 = PasswordField(
        "Repeat Password", validators=[DataRequired(), EqualTo("password")]
    )
    # recaptcha = RecaptchaField("REGISTER")
    submit = SubmitField("REGISTER")

    def validate_username(self, username):
        user = Users.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError("Please use a different username.")

    def validate_email(self, email):
        user = Users.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError("Please use a different email address.")


class AddPropLit(FlaskForm):
    judul = StringField("Judul", validators=[DataRequired()])
    biaya = StringField("Biaya", validators=[DataRequired()])
    tahun = StringField("Tahun", validators=[DataRequired()])
    anggota = StringField("Anggota tim", validators=[DataRequired()])
    kategori = SelectField(
        u"Kategori",
        choices=[
            ("Dasar", "Penelitian Dasar"),
            ("Terapan", "Penelitian Terapan"),
            ("Pengembangan", "Penelitian Pengembangan"),
        ],
    )
    tema = SelectField(
        u"Tema Utama",
        choices=[
            ("BIM", "Building Information Modeling (BIM)"),
            ("Tunnel", "Tunnel"),
            ("Metode Konstruksi", "Metode Konstruksi"),
            ("Lainnya", "Lainnya"),
        ],
    )
    koderumpun = StringField("Kode Rumpun")
    namarumpun = StringField("Nama Rumpun")
    urifile = FileField(
        "Upload proposal (maksimal ukuran 5 MB, .pdf / .docx)",
        validators=[FileRequired()],
    )
    submit = SubmitField("USULKAN")


class UpdatePropLit(FlaskForm):
    judul = StringField("Judul", validators=[DataRequired()])
    biaya = StringField("Biaya", validators=[DataRequired()])
    tahun = StringField("Tahun", validators=[DataRequired()])
    anggota = StringField("Anggota tim", validators=[DataRequired()])
    kategori = SelectField(
        u"Kategori",
        choices=[
            ("Dasar", "Penelitian Dasar"),
            ("Terapan", "Penelitian Terapan"),
            ("Pengembangan", "Penelitian Pengembangan"),
        ],
    )
    tema = SelectField(
        u"Tema Utama",
        choices=[
            ("BIM", "Building Information Modeling (BIM)"),
            ("Tunnel", "Tunnel"),
            ("Metode Konstruksi", "Metode Konstruksi"),
            ("Lainnya", "Lainnya"),
        ],
    )
    koderumpun = StringField("Kode Rumpun")
    namarumpun = StringField("Nama Rumpun")
    urifile = FileField(
        "Upload proposal (maksimal ukuran 5 MB, .pdf / .docx)",
        validators=[FileRequired()],
    )
    submit = SubmitField("UPDATE")


class AddPropPkm(FlaskForm):
    judul = StringField("Judul", validators=[DataRequired()])
    biaya = StringField("Biaya", validators=[DataRequired()])
    tahun = StringField("Tahun", validators=[DataRequired()])
    anggota = StringField("Anggota tim", validators=[DataRequired()])
    urifile = FileField(
        "Upload proposal (maksimal ukuran 5 MB, .pdf / .docx)",
        validators=[FileRequired()],
    )
    submit = SubmitField("USULKAN")


class UpdatePropPkm(FlaskForm):
    judul = StringField("Judul", validators=[DataRequired()])
    biaya = StringField("Biaya", validators=[DataRequired()])
    tahun = StringField("Tahun", validators=[DataRequired()])
    anggota = StringField("Anggota tim", validators=[DataRequired()])
    urifile = FileField(
        "Upload proposal (maksimal ukuran 5 MB, .pdf / .docx)",
        validators=[FileRequired()],
    )
    submit = SubmitField("UPDATE")


class AddPropPubHKI(FlaskForm):
    judul = StringField("Judul", validators=[DataRequired()])
    biaya = StringField("Biaya", validators=[DataRequired()])
    keterangan = TextAreaField("Keterangan", validators=[DataRequired()])
    urifilebukti = FileField(
        "Upload dokumen kelengkapan (maksimal ukuran 5 MB, .pdf / .docx)",
        validators=[FileRequired()],
    )
    submit = SubmitField("USULKAN")


class UpdatePropPubHKI(FlaskForm):
    judul = StringField("Judul", validators=[DataRequired()])
    biaya = StringField("Biaya", validators=[DataRequired()])
    keterangan = TextAreaField("Keterangan", validators=[DataRequired()])
    urifilebukti = FileField(
        "Upload dokumen kelengkapan (maksimal ukuran 5 MB, .pdf / .docx)",
        validators=[FileRequired()],
    )
    submit = SubmitField("UPDATE")


# ADMIN FORMS
## Add User
class AdmAddUsers(FlaskForm):
    username = StringField("Username", validators=[DataRequired()])
    email = StringField("Email", validators=[DataRequired(), Email()])
    name = StringField("Nama", validators=[DataRequired()])
    role = SelectField(
        u"Posisi",
        choices=[("Dosen", "Dosen"), ("UPPM", "UPPM"), ("Penilai", "Penilai")],
    )
    password = PasswordField("Password", validators=[DataRequired()])
    submit = SubmitField("ADD USER")

    def validate_username(self, username):
        user = Users.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError("Please use a different username.")

    def validate_email(self, email):
        user = Users.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError("Please use a different email address.")


## Edit Users
class AdmEditUsers(FlaskForm):
    username = StringField("Username", validators=[DataRequired()])
    email = StringField("Email", validators=[DataRequired()])
    name = StringField("Nama", validators=[DataRequired()])
    role = SelectField(
        u"Posisi",
        choices=[("Dosen", "Dosen"), ("UPPM", "UPPM"), ("Penilai", "Penilai")],
    )
    # karena error di Boolean value, entah kenapa 0 dan 1 dianggap salah jg padahal di docs sqlalchemy value harus 0 atau 1, kita buat spt ini
    togglepenilai = SelectField(
        u"Penilai aktif?", choices=[("Ya", "Ya"), ("Tidak", "Tidak")]
    )
    admin = SelectField(u"Admin?", choices=[("Ya", "Ya"), ("Tidak", "Tidak")])
    submit = SubmitField("UPDATE")

    def __init__(self, original_username, *args, **kwargs):
        super(AdmEditUsers, self).__init__(*args, **kwargs)
        self.original_username = original_username

    def validate_username(self, username):
        if username.data != self.original_username:
            user = Users.query.filter_by(username=self.username.data).first()
            if user is not None:
                raise ValidationError("Please use a different username.")


## Buat pengumuman
class AdmBuatPengumuman(FlaskForm):
    judul = StringField("Judul pengumuman", validators=[DataRequired()])
    isi = TextAreaField("Isi pengumuman", validators=[DataRequired()])
    urifile = FileField("Upload surat/pengumuman (dalam format .jpg)")
    isactive = SelectField(u"Set aktif?", choices=[("Ya", "Ya"), ("Tidak", "Tidak")])
    submit = SubmitField("BUAT")


## Edit pengumuman
class AdmEditPengumuman(FlaskForm):
    judul = StringField("Judul pengumuman", validators=[DataRequired()])
    isi = TextAreaField("Isi pengumuman", validators=[DataRequired()])
    urifile = FileField("Upload surat/pengumuman (dalam format .jpg)")
    isactive = SelectField(u"Set aktif?", choices=[("Ya", "Ya"), ("Tidak", "Tidak")])
    submit = SubmitField("UPDATE")


## Upload file
class AdmUploadFile(FlaskForm):
    namafile = StringField(
        "Nama file (format nama file tanpa ekstensi dan gunakan underscore untuk menggantikan spasi)",
        validators=[DataRequired()],
    )
    urifile = FileField("Upload file")
    submit = SubmitField("UPLOAD")


# PENILAI FORMS
## Penilaian proposal lit dasar
class PenBuatNilaiLitDasar(FlaskForm):
    nrumusanmslh = StringField("Nilai rumusan masalah", validators=[DataRequired()])
    nluaran = StringField("Nilai luaran", validators=[DataRequired()])
    npustaka = StringField("Nilai pustaka", validators=[DataRequired()])
    nmetode = StringField("Nilai metode", validators=[DataRequired()])
    nkelayakan = StringField("Nilai kelayakan", validators=[DataRequired()])
    catatan = StringField("Catatan", validators=[DataRequired()])
    submit = SubmitField("SIMPAN")


## Edit nilai lit dasar
class EditNilaiLitDasar(FlaskForm):
    nrumusanmslh = StringField("Nilai rumusan masalah", validators=[DataRequired()])
    nluaran = StringField("Nilai luaran", validators=[DataRequired()])
    npustaka = StringField("Nilai pustaka", validators=[DataRequired()])
    nmetode = StringField("Nilai metode", validators=[DataRequired()])
    nkelayakan = StringField("Nilai kelayakan", validators=[DataRequired()])
    catatan = StringField("Catatan", validators=[DataRequired()])
    submit = SubmitField("UPDATE")


## Penilaian proposal lit terapan
class PenBuatNilaiLitTerap(FlaskForm):
    nroadmap = StringField("Nilai Roadmap", validators=[DataRequired()])
    nluaran = StringField("Nilai luaran", validators=[DataRequired()])
    npustaka = StringField("Nilai pustaka", validators=[DataRequired()])
    nmetode = StringField("Nilai metode", validators=[DataRequired()])
    nkelayakan = StringField("Nilai kelayakan", validators=[DataRequired()])
    catatan = TextAreaField("Catatan", validators=[DataRequired()])
    submit = SubmitField("SIMPAN")


## Edit nilai lit terapan
class EditNilaiLitTerap(FlaskForm):
    nroadmap = StringField("Nilai Roadmap", validators=[DataRequired()])
    nluaran = StringField("Nilai luaran", validators=[DataRequired()])
    npustaka = StringField("Nilai pustaka", validators=[DataRequired()])
    nmetode = StringField("Nilai metode", validators=[DataRequired()])
    nkelayakan = StringField("Nilai kelayakan", validators=[DataRequired()])
    catatan = TextAreaField("Catatan", validators=[DataRequired()])
    submit = SubmitField("UPDATE")


## Penilaian proposal PkM
class PenBuatNilaiPkm(FlaskForm):
    nsituasi = StringField("Nilai situasi", validators=[DataRequired()])
    nmitra = StringField("Nilai mitra", validators=[DataRequired()])
    nsolusi = StringField("Nilai solusi", validators=[DataRequired()])
    nluaran = StringField("Nilai luaran", validators=[DataRequired()])
    nkelayakan = StringField("Nilai kelayakan", validators=[DataRequired()])
    nbiaya = StringField("Nilai biaya", validators=[DataRequired()])
    catatan = TextAreaField("Catatan", validators=[DataRequired()])
    submit = SubmitField("SIMPAN")


## Edit nilai PkM
class EditNilaiPkm(FlaskForm):
    nsituasi = StringField("Nilai situasi", validators=[DataRequired()])
    nmitra = StringField("Nilai mitra", validators=[DataRequired()])
    nsolusi = StringField("Nilai solusi", validators=[DataRequired()])
    nluaran = StringField("Nilai luaran", validators=[DataRequired()])
    nkelayakan = StringField("Nilai kelayakan", validators=[DataRequired()])
    nbiaya = StringField("Nilai biaya", validators=[DataRequired()])
    catatan = TextAreaField("Catatan", validators=[DataRequired()])
    submit = SubmitField("UPDATE")


## Set status proposal
class SetStatusProp(FlaskForm):
    propstatus = SelectField(
        u"Set status proposal",
        choices=[
            ("Perlu revisi", "Perlu revisi"),
            ("Sudah dinilai", "Sudah dinilai"),
            ("Ditolak", "Ditolak"),
        ],
    )
    submit = SubmitField("SIMPAN")


# FORM PENILAIAN PROPOSAL VERSI BARU (SKOR BASED)
## Penilaian proposal lit dasar (versi baru)
class SkorPropDasarForm(FlaskForm):
    skorrelevansirip = StringField(
        "Skor relevansi RIP",
        validators=[
            DataRequired(),
            AnyOf(
                values=["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skormetode = StringField(
        "Skor metode",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorluaran = StringField(
        "Skor luaran",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorpustaka = StringField(
        "Skor pustaka",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorkelayakan = StringField(
        "Skor kelayakan",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorketerlibatanmhs = StringField(
        "Keterlibatan Mhs",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    catatan = TextAreaField("Catatan", validators=[DataRequired()])
    submit = SubmitField("SIMPAN")


## Edit nilai lit dasar (versi baru)
class EditSkorPropDasarForm(FlaskForm):
    skorrelevansirip = StringField(
        "Skor relevansi RIP",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skormetode = StringField(
        "Skor metode",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorluaran = StringField(
        "Skor luaran",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorpustaka = StringField(
        "Skor pustaka",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorkelayakan = StringField(
        "Skor kelayakan",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorketerlibatanmhs = StringField(
        "Keterlibatan Mhs",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    catatan = TextAreaField("Catatan", validators=[DataRequired()])
    submit = SubmitField("UPDATE")


## Penilaian proposal lit terapan (versi baru)
class SkorPropTerapanForm(FlaskForm):
    skorrelevansirip = StringField(
        "Skor relevansi RIP",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skormetode = StringField(
        "Skor metode",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorluaran = StringField(
        "Skor luaran",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorpustaka = StringField(
        "Skor pustaka",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorkelayakan = StringField(
        "Skor kelayakan",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skormitra = StringField(
        "Skor mitra",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorketerlibatanmhs = StringField(
        "Keterlibatan Mhs",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    catatan = TextAreaField("Catatan", validators=[DataRequired()])
    submit = SubmitField("SIMPAN")


## Edit nilai lit terapan (versi baru)
class EditSkorPropTerapanForm(FlaskForm):
    skorrelevansirip = StringField(
        "Skor relevansi RIP",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skormetode = StringField(
        "Skor metode",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorluaran = StringField(
        "Skor luaran",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorpustaka = StringField(
        "Skor pustaka",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorkelayakan = StringField(
        "Skor kelayakan",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skormitra = StringField(
        "Skor mitra",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorketerlibatanmhs = StringField(
        "Keterlibatan Mhs",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    catatan = TextAreaField("Catatan", validators=[DataRequired()])
    submit = SubmitField("UPDATE")


## Penilaian proposal lit pengembangan (versi baru)
class SkorPropPengembanganForm(FlaskForm):
    skorrelevansirip = StringField(
        "Skor relevansi RIP",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skormetode = StringField(
        "Skor metode",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorluaran = StringField(
        "Skor luaran",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorpustaka = StringField(
        "Skor pustaka",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorkelayakan = StringField(
        "Skor kelayakan",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skormitra = StringField(
        "Skor mitra",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorketerlibatanmhs = StringField(
        "Keterlibatan Mhs",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    catatan = TextAreaField("Catatan", validators=[DataRequired()])
    submit = SubmitField("SIMPAN")


## Edit nilai lit pengembangan (versi baru)
class EditSkorPropPengembanganForm(FlaskForm):
    skorrelevansirip = StringField(
        "Skor relevansi RIP",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skormetode = StringField(
        "Skor metode",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorluaran = StringField(
        "Skor luaran",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorpustaka = StringField(
        "Skor pustaka",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorkelayakan = StringField(
        "Skor kelayakan",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skormitra = StringField(
        "Skor mitra",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorketerlibatanmhs = StringField(
        "Keterlibatan Mhs",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    catatan = TextAreaField("Catatan", validators=[DataRequired()])
    submit = SubmitField("UPDATE")


## Penilaian proposal PkM (versi baru)
class SkorPropPkmForm(FlaskForm):
    skorsituasi = StringField(
        "Skor rumusan",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skormitra = StringField(
        "Skor manfaat",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorsolusi = StringField(
        "Skor solusi",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorluaran = StringField(
        "Skor luaran",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorkelayakan = StringField(
        "Skor kelayakan",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorbiaya = StringField(
        "Skor biaya",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    catatan = TextAreaField("Catatan", validators=[DataRequired()])
    submit = SubmitField("SIMPAN")


## Edit nilai PkM (versi baru)
class EditSkorPropPkmForm(FlaskForm):
    skorsituasi = StringField(
        "Skor rumusan",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skormitra = StringField(
        "Skor manfaat",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorsolusi = StringField(
        "Skor solusi",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorluaran = StringField(
        "Skor luaran",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorkelayakan = StringField(
        "Skor kelayakan",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    skorbiaya = StringField(
        "Skor biaya",
        validators=[
            DataRequired(),
            AnyOf(
                ["1", "2", "3", "5", "6", "7"],
                message="Please enter skor 1 - 7.",
                values_formatter=None,
            ),
        ],
    )
    catatan = TextAreaField("Catatan", validators=[DataRequired()])
    submit = SubmitField("UPDATE")
