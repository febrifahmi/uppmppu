"""empty message

Revision ID: 9b03f55f11b2
Revises: 
Create Date: 2021-04-09 14:37:43.466013

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '9b03f55f11b2'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('users',
    sa.Column('idusers', sa.Integer(), nullable=False),
    sa.Column('username', sa.String(length=64), nullable=True),
    sa.Column('email', sa.String(length=120), nullable=True),
    sa.Column('name', sa.String(length=96), nullable=True),
    sa.Column('password_hash', sa.String(length=128), nullable=True),
    sa.Column('role', sa.String(length=45), nullable=True),
    sa.Column('togglepenilai', sa.String(length=5), nullable=False),
    sa.Column('admin', sa.String(length=5), nullable=False),
    sa.Column('created_at', sa.DateTime(), nullable=False),
    sa.Column('updated_at', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('idusers')
    )
    op.create_index(op.f('ix_users_email'), 'users', ['email'], unique=True)
    op.create_index(op.f('ix_users_username'), 'users', ['username'], unique=True)
    op.create_table('pengumuman',
    sa.Column('idpengumuman', sa.Integer(), nullable=False),
    sa.Column('judul', sa.String(length=256), nullable=False),
    sa.Column('isi', sa.String(length=1000), nullable=False),
    sa.Column('urifile', sa.String(length=512), nullable=True),
    sa.Column('filename', sa.String(length=128), nullable=True),
    sa.Column('isactive', sa.String(length=3), nullable=True),
    sa.Column('created_at', sa.DateTime(), nullable=False),
    sa.Column('updated_at', sa.DateTime(), nullable=True),
    sa.Column('fk_author_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['fk_author_id'], ['users.idusers'], ),
    sa.PrimaryKeyConstraint('idpengumuman')
    )
    op.create_table('proposallit',
    sa.Column('idproposallit', sa.Integer(), nullable=False),
    sa.Column('judul', sa.String(length=1000), nullable=False),
    sa.Column('biaya', sa.Integer(), nullable=True),
    sa.Column('tahun', sa.Integer(), nullable=True),
    sa.Column('anggota', sa.String(length=1000), nullable=True),
    sa.Column('kategori', sa.String(length=45), nullable=True),
    sa.Column('tema', sa.String(length=45), nullable=True),
    sa.Column('koderumpun', sa.Integer(), nullable=True),
    sa.Column('namarumpun', sa.String(length=45), nullable=True),
    sa.Column('qrcodestring', sa.String(length=128), nullable=True),
    sa.Column('urifile', sa.String(length=512), nullable=True),
    sa.Column('filename', sa.String(length=128), nullable=True),
    sa.Column('created_at', sa.DateTime(), nullable=False),
    sa.Column('updated_at', sa.DateTime(), nullable=True),
    sa.Column('status', sa.String(length=45), nullable=True),
    sa.Column('dinilai', sa.String(length=45), nullable=True),
    sa.Column('fk_ketua_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['fk_ketua_id'], ['users.idusers'], ),
    sa.PrimaryKeyConstraint('idproposallit')
    )
    op.create_table('proposalpkm',
    sa.Column('idproposalpkm', sa.Integer(), nullable=False),
    sa.Column('judul', sa.String(length=1000), nullable=False),
    sa.Column('biaya', sa.Integer(), nullable=True),
    sa.Column('tahun', sa.Integer(), nullable=True),
    sa.Column('anggota', sa.String(length=1000), nullable=True),
    sa.Column('qrcodestring', sa.String(length=128), nullable=True),
    sa.Column('urifile', sa.String(length=512), nullable=True),
    sa.Column('filename', sa.String(length=128), nullable=True),
    sa.Column('created_at', sa.DateTime(), nullable=False),
    sa.Column('updated_at', sa.DateTime(), nullable=True),
    sa.Column('status', sa.String(length=45), nullable=True),
    sa.Column('dinilai', sa.String(length=45), nullable=True),
    sa.Column('fk_ketua_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['fk_ketua_id'], ['users.idusers'], ),
    sa.PrimaryKeyConstraint('idproposalpkm')
    )
    op.create_table('publikasihki',
    sa.Column('idpublikasihki', sa.Integer(), nullable=False),
    sa.Column('judul', sa.String(length=1000), nullable=False),
    sa.Column('biaya', sa.Integer(), nullable=True),
    sa.Column('keterangan', sa.String(length=1000), nullable=True),
    sa.Column('urifilebukti', sa.String(length=512), nullable=True),
    sa.Column('filename', sa.String(length=128), nullable=True),
    sa.Column('created_at', sa.DateTime(), nullable=False),
    sa.Column('updated_at', sa.DateTime(), nullable=True),
    sa.Column('status', sa.String(length=45), nullable=True),
    sa.Column('dinilai', sa.String(length=45), nullable=True),
    sa.Column('fk_pengusul_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['fk_pengusul_id'], ['users.idusers'], ),
    sa.PrimaryKeyConstraint('idpublikasihki')
    )
    op.create_table('skorpropdasar',
    sa.Column('idskorpropdasar', sa.Integer(), nullable=False),
    sa.Column('skorrelevansirip', sa.Integer(), nullable=True),
    sa.Column('skormetode', sa.Integer(), nullable=True),
    sa.Column('skorluaran', sa.Integer(), nullable=True),
    sa.Column('skorpustaka', sa.Integer(), nullable=True),
    sa.Column('skorkelayakan', sa.Integer(), nullable=True),
    sa.Column('skorketerlibatanmhs', sa.Integer(), nullable=True),
    sa.Column('catatan', sa.String(length=1000), nullable=True),
    sa.Column('nilaitotal', sa.Integer(), nullable=True),
    sa.Column('created_at', sa.DateTime(), nullable=False),
    sa.Column('updated_at', sa.DateTime(), nullable=True),
    sa.Column('fk_proposal_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['fk_proposal_id'], ['proposallit.idproposallit'], ),
    sa.PrimaryKeyConstraint('idskorpropdasar')
    )
    op.create_table('skorproppengembangan',
    sa.Column('idskorproppengembangan', sa.Integer(), nullable=False),
    sa.Column('skorrelevansirip', sa.Integer(), nullable=True),
    sa.Column('skormetode', sa.Integer(), nullable=True),
    sa.Column('skorluaran', sa.Integer(), nullable=True),
    sa.Column('skorpustaka', sa.Integer(), nullable=True),
    sa.Column('skorkelayakan', sa.Integer(), nullable=True),
    sa.Column('skormitra', sa.Integer(), nullable=True),
    sa.Column('skorketerlibatanmhs', sa.Integer(), nullable=True),
    sa.Column('catatan', sa.String(length=1000), nullable=True),
    sa.Column('nilaitotal', sa.Integer(), nullable=True),
    sa.Column('created_at', sa.DateTime(), nullable=False),
    sa.Column('updated_at', sa.DateTime(), nullable=True),
    sa.Column('fk_proposal_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['fk_proposal_id'], ['proposallit.idproposallit'], ),
    sa.PrimaryKeyConstraint('idskorproppengembangan')
    )
    op.create_table('skorproppkm',
    sa.Column('idskorproppkm', sa.Integer(), nullable=False),
    sa.Column('skorsituasi', sa.Integer(), nullable=True),
    sa.Column('skormitra', sa.Integer(), nullable=True),
    sa.Column('skorsolusi', sa.Integer(), nullable=True),
    sa.Column('skorluaran', sa.Integer(), nullable=True),
    sa.Column('skorkelayakan', sa.Integer(), nullable=True),
    sa.Column('skorbiaya', sa.Integer(), nullable=True),
    sa.Column('catatan', sa.String(length=1000), nullable=True),
    sa.Column('nilaitotal', sa.Integer(), nullable=True),
    sa.Column('created_at', sa.DateTime(), nullable=False),
    sa.Column('updated_at', sa.DateTime(), nullable=True),
    sa.Column('fk_proposal_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['fk_proposal_id'], ['proposalpkm.idproposalpkm'], ),
    sa.PrimaryKeyConstraint('idskorproppkm')
    )
    op.create_table('skorpropterapan',
    sa.Column('idskorpropterapan', sa.Integer(), nullable=False),
    sa.Column('skorrelevansirip', sa.Integer(), nullable=True),
    sa.Column('skormetode', sa.Integer(), nullable=True),
    sa.Column('skorluaran', sa.Integer(), nullable=True),
    sa.Column('skorpustaka', sa.Integer(), nullable=True),
    sa.Column('skorkelayakan', sa.Integer(), nullable=True),
    sa.Column('skormitra', sa.Integer(), nullable=True),
    sa.Column('skorketerlibatanmhs', sa.Integer(), nullable=True),
    sa.Column('catatan', sa.String(length=1000), nullable=True),
    sa.Column('nilaitotal', sa.Integer(), nullable=True),
    sa.Column('created_at', sa.DateTime(), nullable=False),
    sa.Column('updated_at', sa.DateTime(), nullable=True),
    sa.Column('fk_proposal_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['fk_proposal_id'], ['proposallit.idproposallit'], ),
    sa.PrimaryKeyConstraint('idskorpropterapan')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('skorpropterapan')
    op.drop_table('skorproppkm')
    op.drop_table('skorproppengembangan')
    op.drop_table('skorpropdasar')
    op.drop_table('publikasihki')
    op.drop_table('proposalpkm')
    op.drop_table('proposallit')
    op.drop_table('pengumuman')
    op.drop_index(op.f('ix_users_username'), table_name='users')
    op.drop_index(op.f('ix_users_email'), table_name='users')
    op.drop_table('users')
    # ### end Alembic commands ###
