# UPPMOnline Politeknik PU Semarang

Temporary SI for UPPM Poltek PU build using Python3.8, Flask, Bootstrap 5, and Ionicons.

## Setup

1. Install Python3.8 and pip3
2. Install virtualenv and activate
3. Install Flask and module dependencies using pip3 inside virtualenv
4. Set SECRET_KEY in environment before running web app
5. Create .flaskenv file in project root 
6. Setup database (mysql, postgresql)

## Change to MySQL database

1. pip3 install mysql-connector-python
2. ensure the `DATABASE_URL` configuration in env is `mysql+mysqlconnector://<user>:<pass>@<server>/<db>`
2. run `flask db init`
3. run `flask db migrate`
4. run `flask db upgrade`

### Module Dependencies
|Package|Version|
|-------|------:|
|alembic| 1.5.4 |
|attrs| 20.3.0 |
|beautifulsoup4|4.9.3|
|cairocffi|1.2.0|
|CairoSVG|2.5.1|
|certifi|2020.12.5|
|cffi|1.14.4|
|chardet|4.0.0|
|click|7.1.2|
|coverage|5.4|
|cssselect2|0.4.1|
|defusedxml|0.6.0|
|dnspython|2.1.0|
|email-validator|1.1.2|
|et-xmlfile|1.0.1|
|Flask|1.1.2|
|Flask-Excel|0.0.7|
|Flask-Login|0.5.0|
|Flask-Migrate|2.6.0|
|Flask-QRcode|3.0.0|
|Flask-SQLAlchemy|2.4.4|
|Flask-Testing|0.8.1|
|Flask-WeasyPrint|0.6|
|Flask-WTF|0.14.3|
|html5lib|1.1|
|idna|2.10|
|iniconfig|1.1.1|
|itsdangerous|1.1.0|
|jdcal|1.4.1|
|Jinja2|2.11.3|
|lml|0.1.0|
|lxml|4.6.2|
|Mako|1.1.4|
|MarkupSafe|1.1.1|
|mysql-connector-python|8.0.23|
|numpy|1.20.0|
|openpyxl|3.0.6|
|pandas|1.2.1|
|pdfkit|0.6.1|
|Pillow|8.1.0|
|pip|20.3.3|
|pluggy|0.13.1|
|py|1.10.0|
|pycparser|2.20|
|pyexcel|0.6.6|
|pyexcel-io|0.6.4|
|pyexcel-webio|0.1.4|
|pyexcel-xls|0.6.2|
|pyexcel-xlsx|0.6.0|
|pyparsing|2.4.7|
|Pyphen|0.10.0|
|python-dateutil|2.8.1|
|python-dotenv|0.15.0|
|python-editor|1.0.4|
|pytz|2021.1|
|qrcode|6.1|
|requests|2.25.1|
|setuptools|51.1.1|
|six|1.15.0|
|soupsieve|2.1|
|SQLAlchemy|1.3.23|
|texttable|1.6.3|
|tinycss2|1.1.0|
|urllib3|1.26.3|
|WeasyPrint|52.2|
|webencodings|0.5.1|
|Werkzeug|1.0.1|
|wheel|0.36.2|
|WTForms|2.3.3|
|xlrd|1.2.0|
|xlwt|1.3.0|

## Testing
Untuk sementara Gitlab CI dinonaktifkan (deleting gitlab ci yml)